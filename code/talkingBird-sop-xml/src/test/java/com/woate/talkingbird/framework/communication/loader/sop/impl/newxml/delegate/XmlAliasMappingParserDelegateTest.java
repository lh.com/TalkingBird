package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.core.AliasNameManager;

public class XmlAliasMappingParserDelegateTest {
	/**
	 * 测试别名映射解析是否成功
	 * @throws Exception
	 */
	@Test
	public void testParser1() throws Exception{
		URI uri = new File("test1/Test_interface.xml").toURI();
		Document document = initDoc(uri);
		XPath xPath = initXpath(uri);
		XmlSession.getSession().setAttribute(Constantes.PATH_STACK, new XPathStack()) ;
		XmlSession.getSession().setAttribute(Constantes.DOCUMENT, document);
		XmlSession.getSession().setAttribute(Constantes.XPATH, xPath);
		XmlSession.getSession().setAttribute(Constantes.URI, uri);
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, AliasNameManager.getSimpleManager());
		XmlSession.getSession().setAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER, AliasNameManager.getSimpleManager());
		XmlAliasMappingParserDelegate parser = new XmlAliasMappingParserDelegate();
		parser.parse();
		System.out.println(XmlSession.getSession().getAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER));
		System.out.println(XmlSession.getSession().getAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER));
	}
	@Test
	public void testParser2() throws Exception{
		URI uri = new File("test1/Test_interface.xml").toURI();
		Document document = initDoc(uri);
		XPath xPath = initXpath(uri);
		XmlSession.getSession().setAttribute(Constantes.PATH_STACK, new XPathStack()) ;
		XmlSession.getSession().setAttribute(Constantes.DOCUMENT, document);
		XmlSession.getSession().setAttribute(Constantes.XPATH, xPath);
		XmlSession.getSession().setAttribute(Constantes.URI, uri);
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, AliasNameManager.getSimpleManager());
		XmlSession.getSession().setAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER, AliasNameManager.getSimpleManager());
		XmlAliasMappingParserDelegate parser = new XmlAliasMappingParserDelegate();
		parser.parse();
		System.out.println(XmlSession.getSession().getAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER));
		System.out.println(XmlSession.getSession().getAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER));
	}
	
	Document initDoc(URI uri) throws ParserConfigurationException, MalformedURLException, SAXException, IOException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(false);
		factory.setValidating(false);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(uri.toURL().openStream());
		return document;
	}
	
	XPath initXpath(URI uri){
		XPathFactory xfactory = XPathFactory.newInstance();
		XPath xpath = xfactory.newXPath();
		return xpath;
	}
}
