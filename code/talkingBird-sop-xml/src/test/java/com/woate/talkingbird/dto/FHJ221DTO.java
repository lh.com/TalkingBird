package com.woate.talkingbird.dto;

import java.math.BigDecimal;
import java.util.Date;

public class FHJ221DTO {
	BigDecimal closePrice;
	Date date;
	BigDecimal unit;
	public BigDecimal getClosePrice() {
		return closePrice;
	}
	public void setClosePrice(BigDecimal closePrice) {
		this.closePrice = closePrice;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public BigDecimal getUnit() {
		return unit;
	}
	public void setUnit(BigDecimal unit) {
		this.unit = unit;
	}
	
}
