package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util.XmlTester;
import com.woate.talkingbird.framework.core.AliasNameManager;

public class XmlHeaderParserDelegateTest {
	@Test
	public void testParse1() throws Exception{
		URI uri = new File("test1/busi.xml").toURI();
		Node node = XmlTester.getNode(uri, "interfaces");
		Document document = initDoc(uri);
		XPath xPath = initXpath(uri);
		XmlSession.getSession().setAttribute(Constantes.PATH_STACK, new XPathStack()) ;
		XmlSession.getSession().setAttribute(Constantes.DOCUMENT, document);
		XmlSession.getSession().setAttribute(Constantes.XPATH, xPath);
		XmlSession.getSession().setAttribute(Constantes.URI, uri);
		XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, node);
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, AliasNameManager.getSimpleManager());
		AliasNameManager aliasNameManager = AliasNameManager.getSimpleManager();
		aliasNameManager.register("String", String.class.getName());
		XmlSession.getSession().setAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER, aliasNameManager);
		XmlAliasMappingParserDelegate aliasMappingParser = new XmlAliasMappingParserDelegate();
		aliasMappingParser.parse();
		XmlMetadatasPoolParserDelegate metadatasPoolParser = new XmlMetadatasPoolParserDelegate();
		metadatasPoolParser.parse();
		XmlHeaderParserDelegate parser = new XmlHeaderParserDelegate();
		parser.parse();
		Map map =XmlSession.getSession().getAttribute(Constantes.CURRENT_HEADER);
		System.out.println(map);
		
		System.out.println(XmlSession.getSession().getAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER));
		System.out.println(XmlSession.getSession().getAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER));
	}
	
	
	Document initDoc(URI uri) throws ParserConfigurationException, MalformedURLException, SAXException, IOException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(false);
		factory.setValidating(false);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(uri.toURL().openStream());
		return document;
	}
	
	XPath initXpath(URI uri){
		XPathFactory xfactory = XPathFactory.newInstance();
		XPath xpath = xfactory.newXPath();
		return xpath;
	}
}
