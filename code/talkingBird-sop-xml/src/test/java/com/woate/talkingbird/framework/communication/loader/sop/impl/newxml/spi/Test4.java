package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.spi;

import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.sop.SOPMacGenCallback;
import com.woate.talkingbird.framework.communication.sop.SOPPacket;
import com.woate.talkingbird.framework.communication.sop.SOPPacketSFacade;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.impl.SOPMacVlidateCallbackImpl;
import com.woate.talkingbird.framework.util.Hex;

/**
 * 测试接口test3/test_interface.xml
 * @author liucheng
 *
 */
public class Test4 {
	/**
	 * 测试不定长字段的必填属性desc,min-len,max-len,class,allow-null
	 */
	@Test
	public void test1(){
		try {
			SOPSession.destory();
			XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test4");
			loader.load();
			SOPPacket packet = new SOPPacketSFacade("Test", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVlidateCallbackImpl());
			SOPSession.getSession().createBusiData();
			ByteBuffer byteBuffer = packet.format();
			Hex.write(byteBuffer.array());
//			Hex.write(byteBuffer.array(), new FileOutputStream("Test4/test1.hex"));
			byte[] bytes = Hex.read(new FileInputStream("test4/test1.hex"));
			Arrays.equals(bytes, byteBuffer.array());
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
}
