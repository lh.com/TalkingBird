package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.impl;

import java.io.File;
import java.net.URI;
import java.util.List;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.core.AliasNameManager;

public class XmlHeaderParserImplTest {
	@Test
	public void testParse1(){
		XmlSession.destory();
		AliasNameManager aliasNameManager = AliasNameManager.getSimpleManager();
		aliasNameManager.register("String", String.class.getName());
		XmlSession.getSession().setAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER, aliasNameManager);
		XmlSession.getSession().setAttribute(Constantes.CURRENT_PACKET_TYPE, PacketType.REQ);
		URI uri = new File("test1/busi.xml").toURI();
		XmlHeaderParserImpl parser = new XmlHeaderParserImpl();
		parser.parse(uri);
		List<SOPMetadata> headers = XmlSession.getSession().getAttribute(Constantes.CURRENT_HEADER);
		System.out.println(headers);
	}
}
