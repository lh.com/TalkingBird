package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.spi;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPMacGenCallback;
import com.woate.talkingbird.framework.communication.sop.SOPMacVildateCallback;
import com.woate.talkingbird.framework.communication.sop.SOPPacket;
import com.woate.talkingbird.framework.communication.sop.SOPPacketRFacade;
import com.woate.talkingbird.framework.communication.sop.SOPPacketSFacade;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.util.Hex;

public class Test7 {
	@Test
	public void test1() throws TransformException, FileNotFoundException{
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test7");
		loader.load();
		SOPPacket packet = new SOPPacketSFacade("5675", loader,  new SOPMacGenCallback() {
			@Override
			public void call() {
			}
		}, new SOPMacVildateCallback() {
			@Override
			public void vildate() {
			}
		});
			SOPSession.destory();
			SOPSession session = SOPSession.getSession();
			session.setAttribute(Dictionary.YNGYJG, "1001");
			session.setAttribute(Dictionary.JIO1GY, "901328");
			SOPBusiWrapper busiWrapper = session.createBusiData();
			busiWrapper.setBusidata("TASKID", "MOBILE20150107090006000000629306");
			busiWrapper.setBusidata("ZHJGNO", "1234");
			busiWrapper.setBusidata("YYJGNO", "351801");
			busiWrapper.setBusidata("GUIYDH", "E01100");
			busiWrapper.setBusidata("CLASID", "1103");
			busiWrapper.setBusidata("APPLID", "E01100");
			busiWrapper.setBusidata("SHOJHM", "15510065749");
			busiWrapper.setBusidata("DXINLX", "0");
			busiWrapper.setBusidata("DXINBM", "15");
			busiWrapper.setBusidata("DXINNR", "尊敬的客户:由于您的贵金属交易保证金账户余额严重不足,部分持仓已被平仓处理,投资有风险,请谨慎操作");
			busiWrapper.setBusidata("WAPURL", "");
			ByteBuffer buffer = packet.format();
//			System.out.println(Hex.toDisplayString(buffer.array()));
			Hex.write(buffer.array(), new FileOutputStream("test7/test1.hex"));
	}
	@Test
	public void test2() throws TransformException, FileNotFoundException{
		SOPSession.destory();
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test7");
		loader.load();
		SOPPacket packet = new SOPPacketRFacade("5675", loader,  new SOPMacGenCallback() {
			@Override
			public void call() {
			}
		}, new SOPMacVildateCallback() {
			@Override
			public void vildate() {
			}
		});
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.YNGYJG, "1001");
		session.setAttribute(Dictionary.JIO1GY, "901328");
		SOPBusiWrapper busiWrapper = session.createBusiData();
		busiWrapper.setAttribute(Dictionary.JIAOYM, "5675");
		busiWrapper.setBusidata("TASKID", "MOBILE20150107090006000000629306");
		ByteBuffer buffer = packet.format();
		System.out.println(Hex.toDisplayString(buffer.array()));
		Hex.write(buffer.array(), new FileOutputStream("test7/test2.hex"));
	}
	@Test
	public void test2error() throws TransformException, FileNotFoundException{
		SOPSession.destory();
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test7");
		loader.load();
		SOPPacket packet = new SOPPacketRFacade("5675", loader,  new SOPMacGenCallback() {
			@Override
			public void call() {
			}
		}, new SOPMacVildateCallback() {
			@Override
			public void vildate() {
			}
		});
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.YNGYJG, "1001");
		session.setAttribute(Dictionary.JIO1GY, "901328");
		session.setAttribute(Dictionary.PTCWDH, "GT11111");
		SOPBusiWrapper busiWrapper = session.createBusiData();
		busiWrapper.setAttribute(Dictionary.JIAOYM, "5675");
		Map map = new HashMap();
		map.put("errorNo", "1");
		map.put("errorCode", "GT11111");
		map.put("errorMsg", "这是一个测试");
		busiWrapper.setBusidata("ERR000", map);
		ByteBuffer buffer = packet.format();
		System.out.println(Hex.toDisplayString(buffer.array()));
		Hex.write(buffer.array(), new FileOutputStream("test7/test2.hex"));
	}
	@Test
	public void test3() throws TransformException, FileNotFoundException{
		SOPSession.destory();
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test7");
		loader.load();
		SOPPacket packet = new SOPPacketRFacade("5675", loader,  new SOPMacGenCallback() {
			@Override
			public void call() {
			}
		}, new SOPMacVildateCallback() {
			@Override
			public void vildate() {
			}
		});
		byte[] array = Hex.read(new FileInputStream("test7/test1.hex"));
		Hex.write(array);
		ByteBuffer buffer = ByteBuffer.wrap(array);
		buffer.rewind();
		packet.parse(buffer);
		SOPSession session = SOPSession.getSession();
		System.out.println(session.attributes());
		System.out.println(session.peekFirstBusiData().attributes());
		System.out.println(session.pollFirstBusiData().busidatas());
	}
	@Test
	public void test4() throws TransformException, FileNotFoundException{
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test7");
		loader.load();
		//for (int i = 0; i < 1000; i++) {
			SOPSession.destory();
			SOPPacket packet = new SOPPacketSFacade("5675", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVildateCallback() {
				@Override
				public void vildate() {
				}
			});
			byte[] array = Hex.read(new FileInputStream("test7/test2.hex"));
			//Hex.write(array);
			ByteBuffer buffer = ByteBuffer.wrap(array);
			buffer.rewind();
			packet.parse(buffer);
			SOPSession session = SOPSession.getSession();
			System.out.println(session.peekFirstBusiData().busidatas());
			//System.out.println(session.attributes());
			//System.out.println(session.peekFirstBusiData().attributes());
			//System.out.println(session.pollFirstBusiData().busidatas());
		//}
	}
}
