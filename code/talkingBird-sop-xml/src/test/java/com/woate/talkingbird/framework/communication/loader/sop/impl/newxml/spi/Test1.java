package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.spi;

import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.dto.ERR000DTO;
import com.woate.talkingbird.dto.OHJ190DTO;
import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPMacGenCallback;
import com.woate.talkingbird.framework.communication.sop.SOPPacket;
import com.woate.talkingbird.framework.communication.sop.SOPPacketRFacade;
import com.woate.talkingbird.framework.communication.sop.SOPPacketSFacade;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.impl.SOPMacVlidateCallbackImpl;
import com.woate.talkingbird.framework.util.Hex;
/**
 * 测试接口test1/test_interface.xml
 * @author liucheng
 *
 */
public class Test1 {

	/**
	 * 测试没有数据输入的情况下提示未提供交易数据
	 * @throws TransformException
	 */
	@Test
	public void test1() throws TransformException{

		try {
			SOPSession.destory();
			XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test1");
			loader.load();
			SOPPacket packet = new SOPPacketSFacade("Test", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVlidateCallbackImpl());
			ByteBuffer byteBuffer = packet.format();
			Assert.fail("不该运行到此处");
		} catch (Exception e) {
			e.printStackTrace();
			String msg = e.getMessage();
			Assert.assertEquals("接口[Test]在编排时发生异常！", msg);
		}
	}
	@Test
	public void test2() throws TransformException{

		try {
			
			SOPSession.destory();
			XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test1");
			loader.load();
			SOPPacket packet = new SOPPacketSFacade("Test", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVlidateCallbackImpl());
			SOPBusiWrapper busiWrapper = SOPSession.getSession().createBusiData();
			OHJ190DTO ohj190dto = new OHJ190DTO();
			ohj190dto.setBankNo("234");
			busiWrapper.setBusidata("FIELD1", "123");
			busiWrapper.setBusidata("FIELD2", "345");
			busiWrapper.setBusidata("OHJ190", ohj190dto);
			ByteBuffer buffer = packet.format();
			System.out.println(Hex.toHexString(buffer.array()));
			Assert.fail("不该运行到此处");
		} catch (Exception e) {
			String msg = e.getMessage();
			Assert.assertEquals("接口[Test]在编排时发生异常！", msg);
			
		}
	}
	@Test
	public void test3() throws TransformException{

		try {
			
			SOPSession.destory();
			XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test1");
			loader.load();
			SOPPacket packet = new SOPPacketSFacade("Test", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVlidateCallbackImpl());
			SOPBusiWrapper busiWrapper = SOPSession.getSession().createBusiData();
			Test1DTO ohj190dto = new Test1DTO();
			ohj190dto.setFIELD1("234");
			busiWrapper.setBusidata("FIELD1", "123");
			busiWrapper.setBusidata("FIELD2", "345");
			busiWrapper.setBusidata("OHJ190", ohj190dto);
			ByteBuffer buffer = packet.format();
			//Hex.write(buffer.array(), new FileOutputStream("Test1/1.hex"));
			byte[] bytes = Hex.read(new FileInputStream("test1/test3.hex"));
			Hex.write(bytes);
			Arrays.equals(bytes, buffer.array());
		} catch (Exception e) {
			Assert.fail("不该运行到此处");
		}
	}
	/**
	 * 测试Test1下的接口为接收型编排的情况下,由于字段[SYS_FIELD4]为null,导致无法处理
	 * @throws TransformException
	 */
	@Test
	public void test4() throws TransformException{

		try {
			
			SOPSession.destory();
			XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test1");
			loader.load();
			SOPPacket packet = new SOPPacketRFacade("Test", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVlidateCallbackImpl());
			SOPBusiWrapper busiWrapper = SOPSession.getSession().createBusiData();
			Test1DTO ohj190dto = new Test1DTO();
			ohj190dto.setFIELD3("234");
			busiWrapper.setBusidata("OHJ191", ohj190dto);
			List<Test1DTO> fhj191 = new ArrayList<Test1DTO>();
			busiWrapper.setBusidata("FHJ191", fhj191);
			ByteBuffer buffer = packet.format();
//			Hex.write(buffer.array(), new FileOutputStream("Test1/test3.hex"));
//			byte[] bytes = Hex.read(new FileInputStream("test1/test3.hex"));
//			Hex.write(bytes);
//			Arrays.equals(bytes, buffer.array());
		} catch (Exception e) {
			String msg = e.getMessage();
			Assert.assertEquals("接口[Test]在编排时发生异常！", msg);
		}
	}
	/**
	 * 测试Test1下的接口为接收型编排的情况下,存在字段[SYS_FIELD4]长度不够报错
	 * @throws TransformException
	 */
	@Test
	public void test5() throws TransformException{

		try {
			
			SOPSession.destory();
			XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test1");
			loader.load();
			SOPPacket packet = new SOPPacketRFacade("Test", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVlidateCallbackImpl());
			SOPBusiWrapper busiWrapper = SOPSession.getSession().createBusiData();
			SOPSession.getSession().setAttribute("SYS_FIELD4", "");
			Test1DTO ohj190dto = new Test1DTO();
			ohj190dto.setFIELD3("234");
			busiWrapper.setBusidata("OHJ191", ohj190dto);
			List<Test1DTO> fhj191 = new ArrayList<Test1DTO>();
			busiWrapper.setBusidata("FHJ191", fhj191);
			ByteBuffer buffer = packet.format();
//			Hex.write(buffer.array(), new FileOutputStream("Test1/test3.hex"));
//			byte[] bytes = Hex.read(new FileInputStream("test1/test3.hex"));
//			Hex.write(bytes);
//			Arrays.equals(bytes, buffer.array());
		} catch (Exception e) {
			String msg = e.getMessage();
			Assert.assertEquals("接口[Test]在编排时发生异常！", msg);
		}
	}
	/**
	 * 测试Test1下的接口为接收型编排的情况下,存在字段[SYS_FIELD4]长度不够报错
	 * @throws TransformException
	 */
	@Test
	public void test6() throws TransformException{

		try {
			
			SOPSession.destory();
			XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test1");
			loader.load();
			SOPPacket packet = new SOPPacketRFacade("Test", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVlidateCallbackImpl());
			SOPBusiWrapper busiWrapper = SOPSession.getSession().createBusiData();
			SOPSession.getSession().setAttribute("SYS_FIELD4", "123");
			Test1DTO ohj190dto = new Test1DTO();
			ohj190dto.setFIELD3("234");
			busiWrapper.setBusidata("OHJ191", ohj190dto);
			List<Test1DTO> fhj191 = new ArrayList<Test1DTO>();
			busiWrapper.setBusidata("FHJ191", fhj191);
			ByteBuffer buffer = packet.format();
//			Hex.write(buffer.array(), new FileOutputStream("Test1/test3.hex"));
//			byte[] bytes = Hex.read(new FileInputStream("test1/test3.hex"));
//			Hex.write(bytes);
//			Arrays.equals(bytes, buffer.array());
		} catch (Exception e) {
			String msg = e.getMessage();
			Assert.assertEquals("接口[Test]在编排时发生异常！", msg);
		}
	}
	/**
	 * 测试Test1下的接口为接收型编排的情况下,存在字段[SYS_FIELD4]长度超长报错
	 * @throws TransformException
	 */
	@Test
	public void test7() throws TransformException{

		try {
			
			SOPSession.destory();
			XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test1");
			loader.load();
			SOPPacket packet = new SOPPacketRFacade("Test", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVlidateCallbackImpl());
			SOPBusiWrapper busiWrapper = SOPSession.getSession().createBusiData();
			SOPSession.getSession().setAttribute("SYS_FIELD4", "12345678901");
			Test1DTO ohj190dto = new Test1DTO();
			ohj190dto.setFIELD3("234");
			busiWrapper.setBusidata("OHJ191", ohj190dto);
			List<Test1DTO> fhj191 = new ArrayList<Test1DTO>();
			busiWrapper.setBusidata("FHJ191", fhj191);
			ByteBuffer buffer = packet.format();
//			Hex.write(buffer.array(), new FileOutputStream("Test1/test3.hex"));
//			byte[] bytes = Hex.read(new FileInputStream("test1/test3.hex"));
//			Hex.write(bytes);
//			Arrays.equals(bytes, buffer.array());
		} catch (Exception e) {
			String msg = e.getMessage();
			Assert.assertEquals("接口[Test]在编排时发生异常！", msg);
		}
	}
	/**
	 * 测试Test1下的接口为接收型编排的情况下,错误码不为7个AAAAAAA,但是有没有提供错误信息
	 * @throws TransformException
	 */
	@Test
	public void test9() throws TransformException{

		try {
			
			SOPSession.destory();
			XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test1");
			loader.load();
			SOPPacket packet = new SOPPacketRFacade("Test", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVlidateCallbackImpl());
			SOPBusiWrapper busiWrapper = SOPSession.getSession().createBusiData();
			SOPSession.getSession().setAttribute("SYS_FIELD4", "123456789");
			SOPSession.getSession().setAttribute("COM_FIELD8", "123456789");
			SOPSession.getSession().setAttribute(Dictionary.PTCWDH, "GT12345");
			Test1DTO ohj190dto = new Test1DTO();
			ohj190dto.setFIELD3("234");
			busiWrapper.setBusidata("OHJ191", ohj190dto);
			List<Test1DTO> fhj191 = new ArrayList<Test1DTO>();
			busiWrapper.setBusidata("FHJ191", fhj191);
			ByteBuffer buffer = packet.format();
			Assert.fail("不该运行到此处");
//			Hex.write(buffer.array());
//			Hex.write(buffer.array(), new FileOutputStream("Test1/test3.hex"));
//			byte[] bytes = Hex.read(new FileInputStream("test1/test3.hex"));
//			Hex.write(bytes);
//			Arrays.equals(bytes, buffer.array());
		} catch (Exception e) {
			//e.printStackTrace();
			String msg = e.getMessage();
			Assert.assertEquals("接口[Test]在编排时发生异常！", msg);
		}
	}
	/**
	 * 测试Test1下的接口为接收型编排的情况下,错误码不为7个AAAAAAA,提供错误信息
	 * @throws TransformException
	 */
	@Test
	public void test10() throws TransformException{

		try {
			
			SOPSession.destory();
			XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test1");
			loader.load();
			SOPPacket packet = new SOPPacketRFacade("Test", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVlidateCallbackImpl());
			SOPBusiWrapper busiWrapper = SOPSession.getSession().createBusiData();
			SOPSession.getSession().setAttribute("SYS_FIELD4", "123456789");
			SOPSession.getSession().setAttribute("COM_FIELD8", "123456789");
			SOPSession.getSession().setAttribute(Dictionary.PTCWDH, "GT12345");
			ERR000DTO err000dto = new ERR000DTO();
			err000dto.setErrorCode("GT12345");
			err000dto.setErrorMsg("This is a 错误");
			busiWrapper.setBusidata("ERR000", err000dto);
			ByteBuffer buffer = packet.format();
//			Hex.write(buffer.array());
//			Hex.write(buffer.array(), new FileOutputStream("Test1/test10.hex"));
			byte[] bytes = Hex.read(new FileInputStream("test1/test10.hex"));
			Hex.write(bytes);
			Assert.assertTrue(Arrays.equals(bytes, buffer.array()));
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
}
