package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.xml.xpath.XPath;

import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Node;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSessionFactory;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util.XmlTester;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldBuilder;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldImpl;
import com.woate.talkingbird.framework.core.AliasNameManager;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;

public class XmlFieldParserDelegateTest {
	@Test
	public void testParse1() throws Exception{
		XmlSessionFactory.createSession();
		URI uri = new File("test1/sys.xml").toURI();
		XPath xPath = XmlTester.initXpath(uri);
		XPathStack stack = new XPathStack();
		XmlSession.getSession().setAttribute(Constantes.PATH_STACK, stack) ;
		SOPFixedField field = new SOPFixedFieldBuilder("SYS_FIELD1")
									.setDesc("交易代码")
									.setLength(4)
									.setRightFill(true)
									.setClazz(String.class)
									.setFormatConverter(new String2ByteArrayConverter())
									.setParseConverter(new ByteArray2StringConverter())
									.setValueMode(ValueMode.USER)
									.build();
		Map map = new HashMap();
		map.put(field.getName(), field);
		XmlSession.getSession().setAttribute(Constantes.METADATAS_POOL, map) ;
		XmlSession.getSession().setAttribute(Constantes.XPATH, xPath);
		XmlSession.getSession().setAttribute(Constantes.URI, uri);
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, AliasNameManager.getSimpleManager());
		AliasNameManager aliasNameManager = XmlSession.getSession().getAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER);
		aliasNameManager.register("String", "java.lang.String");
		Node node = XmlTester.getNode(uri, "interfaces/request/field");
		stack.push("interfaces/request/SYS_FIELD1");
		XmlFieldParserDelegate parser = new XmlFieldParserDelegate();
		XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, node);
		parser.parse();
		SOPFixedFieldImpl field0 = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
		Assert.assertEquals("SYS_FIELD1", field0.getName());
		Assert.assertEquals("SYS_FIELD1_REQ", field0.getAlias());
		Assert.assertEquals("交易代码", field0.getDesc());
		Assert.assertEquals(4, field0.getMaxLength());
		Assert.assertEquals(String.class, field0.getClazz());
		System.out.println(field0);
	}
	
	

}
