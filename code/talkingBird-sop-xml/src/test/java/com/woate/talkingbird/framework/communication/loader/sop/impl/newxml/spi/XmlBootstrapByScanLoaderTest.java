package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.spi;

import java.util.Arrays;
import java.util.Collection;



import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.dto.OHJ190DTO;
import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BootstrapMetadataLoader;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPPacket;
import com.woate.talkingbird.framework.communication.sop.SOPPacketRFacade;
import com.woate.talkingbird.framework.communication.sop.SOPPacketSFacade;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.impl.SOPMacGenCallbackImpl;
import com.woate.talkingbird.framework.communication.sop.impl.SOPMacVlidateCallbackImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.util.Hex;

public class XmlBootstrapByScanLoaderTest {
	@Test
	public void testLoad1(){
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test1");
		BootstrapMetadataLoader<SOPMetadata> bootLoader = loader.load();
		System.out.println(bootLoader);
	}
	@Test
	public void testLoad2(){
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test1");
		loader.load();
		Collection<String> list = loader.getInterfaceNames();
		System.out.println(list);
		Assert.assertEquals(1, list.size());
	}
	@Test
	public void testGetLoader1() throws TransformException{
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("interfaces");
		loader.load();
		SOPPacket sopPacket1 = new SOPPacketSFacade("HJ19", loader, new SOPMacGenCallbackImpl(), new SOPMacVlidateCallbackImpl());
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.MACJGH, "9489");
		session.setAttribute(Dictionary.CHNLNO, "489");
		session.setAttribute(Dictionary.CHNLTO, "489");
		session.setAttribute(Dictionary.MACFLG, "0");
		session.setAttribute(Dictionary.PINFLG, "0");
		session.setAttribute(Dictionary.COMFLG, "0");
		session.setAttribute(Dictionary.JIAOYM, "900b");
		session.setAttribute(Dictionary.QUDAOO, "810");
		session.createBusiData();
		OHJ190DTO ohj190dto = new OHJ190DTO();
		ohj190dto.setBankNo("1");
		ohj190dto.setProdId("1");
		session.peekFirstBusiData().setBusidata("OHJ190", ohj190dto);
		session.peekFirstBusiData().setBusidata("bankNo", "22222222222");
		java.nio.ByteBuffer buffer = sopPacket1.format();
		System.out.println(Hex.toHexString(buffer.array()));
		System.out.println(Arrays.toString(buffer.array()));
		SOPPacket sopPacket2 = new SOPPacketRFacade("HJ19", loader, new SOPMacGenCallbackImpl(), new SOPMacVlidateCallbackImpl());
		buffer.rewind();
		sopPacket2.parse(buffer);
		SOPSession session1 = SOPSession.getSession();
		Integer len = session1.getAttribute(Dictionary.SHJBCD);
		String mac = session1.getAttribute(Dictionary.BAWMAC);
		SOPBusiWrapper busi = session1.peekFirstBusiData();
		System.out.println(session1.attributes());
		System.out.println(busi.attributes());
		System.out.println(busi.getBusidata("OHJ190"));
		System.out.println(busi.getBusidata("bankNo"));
	}
}
