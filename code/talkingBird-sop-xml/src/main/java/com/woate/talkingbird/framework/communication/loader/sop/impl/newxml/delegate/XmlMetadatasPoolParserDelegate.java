/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
/**
 * Xml元信息池解析代理类
 * @author liucheng
 *
 */
public class XmlMetadatasPoolParserDelegate implements XmlParser {
	XmlParser fixedFieldParser = new XmlFixedFieldParserDelegate();
	XmlParser VariableFieldParser = new XmlVariableFieldParserDelegate();
	@Override
	public void parse() throws XmlParseException {
		try {
			parseMetadataPool();
		} catch (XmlParseException e) {
			throw e;
		} catch (Exception e) {
			throw new XmlParseException(e);
		}
	}
	
	void parseMetadataPool() throws Exception{
		XPathStack pathStack = XmlSession.getSession().getAttribute(Constantes.PATH_STACK);
		URI uri = XmlSession.getSession().getAttribute(Constantes.URI);
		Document document = XmlSession.getSession().getAttribute(Constantes.DOCUMENT);
		XPath xpath = XmlSession.getSession().getAttribute(Constantes.XPATH);
		pathStack.push(Constantes.INTERFACES_METADATS_POOL);
		XPathExpression metadataPoolExpr = xpath.compile(Constantes.INTERFACES_METADATS_POOL);
		XPathExpression fixedFieldExpr = xpath.compile(Constantes.FIXED_FIELD);
		XPathExpression variableFieldExpr = xpath.compile(Constantes.VARIABLE_FIELD);
		Node poolNode = null;
		try {
			poolNode = (Node) metadataPoolExpr.evaluate(document, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmlParseException("载入[" + uri + "]元信息池失败", e);
		}
		if (poolNode == null) {
			throw new XmlParseException("/interfaces/metadatas-pool 节点不存在");
		}
		NodeList fixedPoolNodeSet = null;
		try {
			fixedPoolNodeSet = (NodeList) fixedFieldExpr.evaluate(poolNode, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new XmlParseException(e);
		}
		NodeList variablePoolNodeSet = null;
		try {
			variablePoolNodeSet = (NodeList) variableFieldExpr.evaluate(poolNode, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new XmlParseException(e);
		}
		Map<String, SOPVariableField> metadatasPool = new HashMap<String, SOPVariableField>();
		parseFixedFields(fixedPoolNodeSet, metadatasPool);
		parseVariableFields(variablePoolNodeSet, metadatasPool);
		XmlSession.getSession().setAttribute(Constantes.METADATAS_POOL, metadatasPool);
		pathStack.poll();
	}

	void parseFixedFields(NodeList poolNodeSet,Map<String, SOPVariableField> metadatasPool) throws Exception{
		for (int i = 0; i < poolNodeSet.getLength(); i++) {
			Node fieldNode  = poolNodeSet.item(i);
			XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, fieldNode);
			fixedFieldParser.parse();
			SOPFixedField field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
			metadatasPool.put(field.getName(), field);
		}
	}
	void parseVariableFields(NodeList poolNodeSet,Map<String, SOPVariableField> metadatasPool) throws Exception{
		for (int i = 0; i < poolNodeSet.getLength(); i++) {
			Node fieldNode  = poolNodeSet.item(i);
			XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, fieldNode);
			VariableFieldParser.parse();
			SOPVariableField field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
			metadatasPool.put(field.getName(), field);
		}
	}
}
