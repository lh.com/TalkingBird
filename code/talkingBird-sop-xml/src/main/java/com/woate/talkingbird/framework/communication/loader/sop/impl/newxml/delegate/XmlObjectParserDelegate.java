/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;
 
import static com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util.XmlNodeVildateUtil.getValueBySafety;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util.FindClassByAliasUtil;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPBeanImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFormImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPBeanWriteable;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPFixedFieldWriteable;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPFormWriteable;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPObjectWriteable;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
/**
 * 解析object节点解析器代理类
 * @author liucheng
 *
 */
public class XmlObjectParserDelegate implements XmlParser{
	XmlParser variableFieldParser = new XmlVariableFieldParserDelegate();
	XmlParser fixedFieldParser = new XmlFixedFieldParserDelegate();
	XmlParser fieldParser = new XmlFieldParserDelegate();
	@Override
	public void parse() throws XmlParseException {
		try {
			SOPObjectWriteable object = parse1(parse0());
			XmlSession.getSession().setAttribute(Constantes.CURRENT_OBJECT,object);
		} catch (XmlParseException e) {
			throw e;
		} catch (Exception e) {
			throw new XmlParseException(e);
		}
	}
	
	SOPObjectWriteable parse0() throws Exception{
		Node objectNode = XmlSession.getSession().getAttribute(Constantes.CURRENT_NODE);
		NamedNodeMap map = objectNode.getAttributes();
		Node nameNode = map.getNamedItem(Constantes.OBJECT_NAME);
		Node aliasNode = map.getNamedItem(Constantes.OBJECT_ALIAS);
		Node descNode = map.getNamedItem(Constantes.OBJECT_DESC);
		Node encodingNode = map.getNamedItem(Constantes.OBJECT_ENCODING);
		Node typeNode = map.getNamedItem(Constantes.OBJECT_TYPE);
		Node wrapClassNode = map.getNamedItem(Constantes.OBJECT_WRAP_CLASS);
		String name = getValueBySafety(nameNode);
		String alias = getValueBySafety(aliasNode);
		String desc = getValueBySafety(descNode);
		String encoding = getValueBySafety(encodingNode);
		String type = getValueBySafety(typeNode);
		String wrapClass = getValueBySafety(wrapClassNode);
		Class<?> wrapClass1 = null;
		try {
			wrapClass1 = FindClassByAliasUtil.findByAlias(wrapClass);
		} catch (Exception e) {
			throw new XmlParseException("处理["+name+"] 对象节点包装类["+ wrapClass +"]发生异常" ,e);
		}
		if(type.equals(Constantes.BEAN)){
			SOPBeanWriteable bean = new SOPBeanImpl(name, alias, desc, encoding, wrapClass1);
			return bean;
		}else if(type.equals(Constantes.FORM)){
			SOPFormWriteable<SOPMetadata> form = new SOPFormImpl<SOPMetadata>(name, alias, desc, encoding, wrapClass1);
			return form;
		}else{			
			return null;
		}
	}
	SOPObjectWriteable parse1(SOPObjectWriteable object){
		Node objectNode = XmlSession.getSession().getAttribute(Constantes.CURRENT_NODE);
		NodeList nodeList = objectNode.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node fieldNode = nodeList.item(i);
			String fieldName = fieldNode.getNodeName();
			XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, fieldNode);
			if(Constantes.FIXED_FIELD.equals(fieldName)){
				fixedFieldParser.parse();
				SOPFixedFieldWriteable field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
				//如果字段没有设置编码集就继承Object节点的编码集
				if(field.getEncoding() == null || field.getEncoding().isEmpty()){
					String objEncoding = object.getEncoding();
					field.setEncoding(objEncoding);
				}
				object.addField(field);
			}else if(Constantes.VARIABLE_FIELD.equals(fieldName)){
				variableFieldParser.parse();
				SOPVariableFieldWriteable field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
				//如果字段没有设置编码集就继承Object节点的编码集
				if(field.getEncoding() == null || field.getEncoding().isEmpty()){
					String objEncoding = object.getEncoding();
					field.setEncoding(objEncoding);
				}
				object.addField(field);
			}else if(Constantes.FIELD.equals(fieldName)){
				fieldParser.parse();
				SOPVariableFieldWriteable field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
				//如果字段没有设置编码集就继承Object节点的编码集
				if(field.getEncoding() == null || field.getEncoding().isEmpty()){
					String objEncoding = object.getEncoding();
					field.setEncoding(objEncoding);
				}
				object.addField(field);
			}
		}
		return object;
	}
}
