/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.impl.XmlSessionImpl;

/**
 * 会话对象工厂类
 *
 * @author liucheng
 * @version 0.1 2013-4-17 下午5:37:11
 */
public class XmlSessionFactory {
	private static ThreadLocal<XmlSession> sessionThreadLocal = new ThreadLocal<XmlSession>();
	/**
	 * 创建会话对象
	 *
	 * @return 会话对象
	 */
	public static XmlSessionImpl createSession() {
		XmlSessionImpl session = (XmlSessionImpl) sessionThreadLocal.get();
		if (session != null) {
			sessionThreadLocal.remove();
		}

		session = new XmlSessionImpl();
		sessionThreadLocal.set(session);
		return session;
	}

	/**
	 * 从线程中获取会话对象
	 *
	 * @return XMl会话对象
	 */
	public static XmlSession getSession() {
		XmlSession session = sessionThreadLocal.get();
		if (session == null) {
			session = createSession();
			sessionThreadLocal.set(session);
		}
		return session;
	}

	/**
	 * 销毁会话
	 */
	public static void destorySession() {
		XmlSession session = sessionThreadLocal.get();
		if (session != null) {
			sessionThreadLocal.remove();
		}
	}
}
