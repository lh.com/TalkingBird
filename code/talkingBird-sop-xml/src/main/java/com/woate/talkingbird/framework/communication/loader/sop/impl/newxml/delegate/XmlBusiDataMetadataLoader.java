/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.core.AliasNameManager;
/**
 * 基于newXml格式的交易元信息加载器实现
 * @author liucheng
 *
 */
public class XmlBusiDataMetadataLoader implements BusiDataMetadateLoader<SOPMetadata>{
	boolean binit = false;
	/**
	 * 头文件名
	 */
	String headFileName;
	/**
	 * 交易码
	 */
	String tradeCode;
	/**
	 * 该别名管理器管理交易数据和交易数据头定义字段的别名
	 */
	AliasNameManager aliasNameManager = AliasNameManager.getSimpleManager();
	/**
	 * 交易数据头别名管理器
	 */
	AliasNameManager headerAliasNameManager = AliasNameManager.getSimpleManager();
	/**
	 * 主加载器
	 */
	MainMetadataLoader<SOPMetadata> mainLoader;
	/**
	 * 头信息
	 */
	final Map<PacketType, List<SOPMetadata>> headers = new HashMap<PacketType, List<SOPMetadata>>();
	/**
	 * 数据信息
	 */
	final Map<PacketType, List<SOPMetadata>> datas = new HashMap<PacketType, List<SOPMetadata>>();

	public XmlBusiDataMetadataLoader(String tradeCode, MainMetadataLoader<SOPMetadata> mainLoader) {
		super();
		this.tradeCode = tradeCode;
		this.mainLoader = mainLoader;
		datas.put(PacketType.REQ, new ArrayList<SOPMetadata>());
		datas.put(PacketType.RSP, new ArrayList<SOPMetadata>());
		datas.put(PacketType.EXP, new ArrayList<SOPMetadata>());
		headers.put(PacketType.REQ, new ArrayList<SOPMetadata>());
		headers.put(PacketType.RSP, new ArrayList<SOPMetadata>());
		headers.put(PacketType.EXP, new ArrayList<SOPMetadata>());
	}

	public String getHeadFileName(){
		return headFileName;
	}
	

	@Override
	public String getTradeCode() {
		return tradeCode;
	}
	
	@Override
	public BusiDataMetadateLoader<SOPMetadata> load() {
		binit = false;
		binit = true;
		return this;
	}

	@Override
	public List<SOPMetadata> list(PacketType type) {
		if (!binit) {
			throw new XmlParseException("XmlBusiDataMetadataLoader is not loaded!");
		}
		if(type == PacketType.REQ || type == PacketType.RSP || type == PacketType.EXP ){
			return datas.get(type);
		}else if(type == PacketType.ALL ){
			List<SOPMetadata> all = new ArrayList<SOPMetadata>();
			all.addAll(datas.get(PacketType.REQ));
			all.addAll(datas.get(PacketType.RSP));
			all.addAll(datas.get(PacketType.EXP));
			return all;
		}
		throw new XmlParseException("["+this.toString()+"]不支持的操作["+type+"]");
	}

	@Override
	public List<SOPMetadata> listHeader(PacketType type) {
		if (!binit) {
			throw new XmlParseException("XmlBusiDataMetadataLoader is not loaded!");
		}
		if(type == PacketType.REQ || type == PacketType.RSP|| type == PacketType.EXP ){
			return headers.get(type);
		}else if(type == PacketType.ALL ){
			List<SOPMetadata> all = new ArrayList<SOPMetadata>();
			all.addAll(headers.get(PacketType.REQ));
			all.addAll(headers.get(PacketType.RSP));
			all.addAll(headers.get(PacketType.EXP));
			return all;
		}
		throw new XmlParseException("["+this.toString()+"]不支持的操作["+type+"]");
	}

	@Override
	public AliasNameManager getAliasNameManager() {
		return aliasNameManager;
	}
	
	@Override
	public AliasNameManager getHeaderAliasNameManager() {
		return headerAliasNameManager;
	}
	

	@Override
	public MainMetadataLoader<SOPMetadata> getMainLoader() {
		return mainLoader;
	}

	@Override
	public String toString() {
		return "XmlBusiDataMetadataLoader [tradeCode=" + tradeCode + ", headers=" + headers + ", datas=" + datas + ", busiAliasNameManager=" + aliasNameManager + "]";
	}

}
