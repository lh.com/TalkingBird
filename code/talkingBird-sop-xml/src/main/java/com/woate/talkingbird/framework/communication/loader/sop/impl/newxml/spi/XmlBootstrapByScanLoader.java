/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.spi;

import java.io.File;
import java.io.FileFilter;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.woate.talkingbird.framework.communication.loader.BootstrapMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlInterfaceParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSessionFactory;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.impl.XmlInterfaceParserImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.core.AliasNameManager;
/**
 * 扫描式根加载器<br>
 * 通过对一个根目录进行扫描加载<br>
 * @author liucheng
 * 2014-12-30 上午11:16:50
 *
 */
public class XmlBootstrapByScanLoader implements BootstrapMetadataLoader<SOPMetadata>{
	URI bootstrapUri;
	boolean binit = false;
	Map<String, MainMetadataLoader<SOPMetadata>> loaders = new HashMap<String, MainMetadataLoader<SOPMetadata>>();
	AliasNameManager aliasNameManager = AliasNameManager.getSimpleManager();
	public XmlBootstrapByScanLoader(String path) {
		super();
		this.bootstrapUri = new File(path).toURI();
	}
	@Override
	public MainMetadataLoader<SOPMetadata> getLoader(String name) {
		if (!binit) {
			throw new XmlParseException("XmlBusiDataMetadataLoader is not loaded!");
		}
		return this.loaders.get(name);
	}

	@Override
	public Collection<String> getInterfaceNames() {
		if (!binit) {
			throw new XmlParseException("XmlBusiDataMetadataLoader is not loaded!");
		}
		return this.loaders.keySet();
	}

	@Override
	public BootstrapMetadataLoader<SOPMetadata> load() {
		XmlSessionFactory.createSession();
		this.binit = false;
		File bootstrapDir = new File(this.bootstrapUri);
		//只扫描.xml结尾的文件
		File[] files = bootstrapDir.listFiles(new FileFilter(){
			@Override
			public boolean accept(File pathname) {
				if(pathname.getName().endsWith("_interface.xml")){					
					return true;
				}else{
					return false;
				}
			}});
		XmlSession.getSession().setAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER, getAliasNameManager());
		XmlSession.getSession().setAttribute(Constantes.CURRENT_BOOTSTRAP_LOADER, this);
		for (File file : files) {
			XmlInterfaceParser parser = new XmlInterfaceParserImpl();
			MainMetadataLoader<SOPMetadata> mainLoader = parser.parse(file.toURI());
			mainLoader.load();
			this.loaders.put(mainLoader.getName(), mainLoader);
		}
		XmlSession.getSession().setAttribute(Constantes.CURRENT_BOOTSTRAP_LOADER, null);
		XmlSession.getSession().setAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER, null);
		XmlSessionFactory.destorySession();
		this.binit = true;
		return this;
	}
	@Override
	public AliasNameManager getAliasNameManager() {
		return aliasNameManager;
	}
	@Override
	public String toString() {
		return "XmlBootstrapByScanLoader [aliasNameManager=" + aliasNameManager + ", loaders=" + loaders + "]";
	}

	
}
