/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlHeaderParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.impl.XmlHeaderParserImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPMetadataWriteable;
import com.woate.talkingbird.framework.core.AliasNameManager;
/**
 * 交易数据节点解析器代理类
 * @author liucheng
 *
 */
public class XmlBusisParserDelegate implements XmlParser {
	XmlParser variableFieldParser = new XmlVariableFieldParserDelegate();
	XmlParser fixedFieldParser = new XmlFixedFieldParserDelegate();
	XmlParser fieldParser = new XmlFieldParserDelegate();
	XmlParser objectParser = new XmlObjectParserDelegate();
	@Override
	public void parse() throws XmlParseException {
		try {
			parse0();
		} catch (XmlParseException e) {
			throw e;
		} catch (Exception e) {
			URI uri = XmlSession.getSession().getAttribute(Constantes.URI);
			throw new XmlParseException("在解析" + uri + "时发生异常",e);
		}
	}

	void parse0() throws XPathExpressionException, ClassNotFoundException {
		Node busisNode = XmlSession.getSession().getAttribute(Constantes.CURRENT_NODE);
		XmlMainMetadataLoader mainLoader = XmlSession.getSession().getAttribute(Constantes.CURRENT_MAIN_LOADER);
		NodeList nodeList = busisNode.getChildNodes();
		XPathStack xPathStack = XmlSession.getSession().getAttribute(Constantes.PATH_STACK);
		XPath xpath = XmlSession.getSession().getAttribute(Constantes.XPATH);
		PacketType type = XmlSession.getSession().getAttribute(Constantes.CURRENT_PACKET_TYPE);
		Map<String, SOPMetadata> metadataPool = XmlSession.getSession().getAttribute(Constantes.METADATAS_POOL);
		// 处理交易节点
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node businode = nodeList.item(i);
			String nodeName = businode.getNodeName();
			if (Constantes.BUSI.equals(nodeName)) {
				String tradeCode = xpath.evaluate("@name", businode);
				XmlBusiDataMetadataLoader busiLoader = mainLoader.getNewOrExist(tradeCode);
				parseBusiHeader0(type, businode, busiLoader);
				XmlSession.getSession().setAttribute(Constantes.METADATAS_POOL, metadataPool);
				parseBusi0(type, businode, busiLoader);
				XmlSession.getSession().setAttribute(Constantes.METADATAS_POOL, null);
			}
		}
	}

	/**
	 * 解析交易数据头
	 * 
	 * @param busiNode
	 * @throws XPathExpressionException
	 * @throws ClassNotFoundException 
	 */
	void parseBusiHeader0(PacketType type, Node busiNode, XmlBusiDataMetadataLoader busiLoader) throws XPathExpressionException, ClassNotFoundException {
		XPathStack xPathStack = XmlSession.getSession().getAttribute(Constantes.PATH_STACK);
		XPath xpath = XmlSession.getSession().getAttribute(Constantes.XPATH);
		// 获取请求节点中的系统信息头
		xPathStack.push(Constantes.HEADER);
		xPathStack.push(Constantes.INLINE);
		String file = xpath.evaluate("header/@inline", busiNode);
		URI headerURI = new File(file).toURI();
		AliasNameManager asliasNameManager = AliasNameManager.getSimpleManager();
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, asliasNameManager);
		//解析出对应的头信息
		XmlHeaderParser headerParser = new XmlHeaderParserImpl();
		List<SOPMetadata> headers = headerParser.parse(headerURI);
		List<SOPMetadata> busiheader = busiLoader.headers.get(type);
		busiheader.addAll(headers);
		int beginPos = file.lastIndexOf("/");
		int endPos = file.lastIndexOf(".");
		busiLoader.headFileName = file.substring(beginPos, endPos);
		for (SOPMetadata sopMetadata : headers) {
			busiLoader.getHeaderAliasNameManager().register(sopMetadata.getAlias(), sopMetadata.getName());
		}
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, null);
		xPathStack.poll();
		xPathStack.poll();
	}

	/**
	 * 解析交易数据
	 * 
	 * @param busiNode
	 * @throws Exception 
	 */
	void parseBusi0(PacketType type, Node busiNode,XmlBusiDataMetadataLoader busiLoader){
		XPathStack xPathStack = XmlSession.getSession().getAttribute(Constantes.PATH_STACK);
		XPath xpath = XmlSession.getSession().getAttribute(Constantes.XPATH);
		XmlMainMetadataLoader mainLoader = XmlSession.getSession().getAttribute(Constantes.CURRENT_MAIN_LOADER);
		List<SOPMetadata> list = busiLoader.datas.get(type);
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, busiLoader.getMainLoader().getAliasNameManager());
		list.addAll(parseBusi1(busiNode));
		//注册交易数据的别名
		for (SOPMetadata sopMetadata : list) {
			busiLoader.getAliasNameManager().register(sopMetadata.getAlias(), sopMetadata.getName());
		}
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, null);
	}
	
	List<SOPMetadata> parseBusi1(Node node){
		List<SOPMetadata> list = new ArrayList<SOPMetadata>();
		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node fieldNode = nodeList.item(i);
			XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, fieldNode);
			String fieldName = fieldNode.getNodeName();
			if(Constantes.FIXED_FIELD.equals(fieldName)){
				fixedFieldParser.parse();
				SOPMetadataWriteable field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
				list.add(field);
			}else if(Constantes.VARIABLE_FIELD.equals(fieldName)){
				variableFieldParser.parse();
				SOPMetadataWriteable field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
				list.add(field);
			}else if(Constantes.FIELD.equals(fieldName)){
				fieldParser.parse();
				SOPMetadataWriteable field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
				list.add(field);
			}else if(Constantes.OBJECT.equals(fieldName)){
				objectParser.parse();
				SOPMetadataWriteable object = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
				if(object.getEncoding() == null || object.getEncoding().isEmpty()){
					object.setEncoding("GBK");
				}
				list.add(object);
			}
		}
		
		return list;
	}

}
