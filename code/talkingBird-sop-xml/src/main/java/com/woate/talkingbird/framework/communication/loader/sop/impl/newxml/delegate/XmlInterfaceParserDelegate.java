/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.io.File;
import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlHeaderParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.impl.XmlHeaderParserImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.core.AliasNameManager;
/**
 * 解析interface解析器代理类
 * @author liucheng
 *
 */
public class XmlInterfaceParserDelegate implements XmlParser{
	XmlParser busisParser = new XmlBusisParserDelegate();
	@Override
	public void parse() throws XmlParseException {
		URI uri = XmlSession.getSession().getAttribute(Constantes.URI);
		try {
			Node tradeNode = XmlSession.getSession().getAttribute(Constantes.CURRENT_NODE);
			NodeList nodeList = tradeNode.getChildNodes();
			XPathStack xPathStack = XmlSession.getSession().getAttribute(Constantes.PATH_STACK);
			XPath xpath = XmlSession.getSession().getAttribute(Constantes.XPATH);
			Map<String, SOPMetadata> metadataPool = XmlSession.getSession().getAttribute(Constantes.METADATAS_POOL);
			//处理请求，应答，异常
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				String nodeName = node.getNodeName();
				if(Constantes.REQUEST.equals(nodeName)){
					XmlSession.getSession().setAttribute(Constantes.CURRENT_PACKET_TYPE, PacketType.REQ);
					parseSys0(PacketType.REQ, node);
					parseCom0(PacketType.REQ, node);
					XmlSession.getSession().setAttribute(Constantes.METADATAS_POOL, metadataPool);
					parseBusis0(PacketType.REQ, node);
					XmlSession.getSession().setAttribute(Constantes.METADATAS_POOL, null);
				}else if(Constantes.RESPONSE.equals(nodeName)){	
					XmlSession.getSession().setAttribute(Constantes.CURRENT_PACKET_TYPE, PacketType.RSP);
					parseSys0(PacketType.RSP, node);
					parseCom0(PacketType.RSP, node);
					XmlSession.getSession().setAttribute(Constantes.METADATAS_POOL, metadataPool);
					parseBusis0(PacketType.RSP, node);
					XmlSession.getSession().setAttribute(Constantes.METADATAS_POOL, null);
				}else if(Constantes.EXCEPTION.equals(nodeName)){
					XmlSession.getSession().setAttribute(Constantes.CURRENT_PACKET_TYPE, PacketType.EXP);
					parseSys0(PacketType.EXP, node);
					parseCom0(PacketType.EXP, node);
					XmlSession.getSession().setAttribute(Constantes.METADATAS_POOL, metadataPool);
					parseBusis0(PacketType.EXP, node);
					XmlSession.getSession().setAttribute(Constantes.METADATAS_POOL, null);
				}
			}
			
		} catch (XmlParseException e) {
			throw new XmlParseException(uri.toString(), e);
		} catch (Exception e) {
			throw new XmlParseException(uri.toString(), e);
		}
		
	}

	/**
	 * 系统信息头
	 * @param node
	 * @throws XPathExpressionException 
	 */
	void parseSys0(PacketType type, Node node) throws XPathExpressionException{
		XPathStack xPathStack = XmlSession.getSession().getAttribute(Constantes.PATH_STACK);
		XPath xpath = XmlSession.getSession().getAttribute(Constantes.XPATH);
		XmlMainMetadataLoader mainLoader = XmlSession.getSession().getAttribute(Constantes.CURRENT_MAIN_LOADER);
		// 获取请求节点中的系统信息头
		xPathStack.push(Constantes.SYS);
		xPathStack.push(Constantes.INLINE);
		String file = xpath.evaluate("sys/@inline", node);
		URI uri = new File(file).toURI();
		AliasNameManager asliasNameManager = AliasNameManager.getSimpleManager();
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, asliasNameManager);
		XmlHeaderParser headerParser = new XmlHeaderParserImpl();
		List<SOPMetadata> headers = headerParser.parse(uri);
		List<SOPMetadata> sysheader = mainLoader.sysheaders.get(type);
		sysheader.addAll(headers);
		int beginPos = file.lastIndexOf("/");
		int endPos = file.lastIndexOf(".");
		mainLoader.sysHeadFileName = file.substring(beginPos, endPos);
		for (SOPMetadata sopMetadata : headers) {
			mainLoader.getSysHeaderLoader().getAliasNameManager().register(sopMetadata.getAlias(), sopMetadata.getName());
		}
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, null);
		xPathStack.poll();
		xPathStack.poll();
	}
	/**
	 * 公共信息头
	 * @param node
	 * @throws XPathExpressionException 
	 */
	void parseCom0(PacketType type, Node node) throws XPathExpressionException{
		XPathStack xPathStack = XmlSession.getSession().getAttribute(Constantes.PATH_STACK);
		XPath xpath = XmlSession.getSession().getAttribute(Constantes.XPATH);
		XmlMainMetadataLoader mainLoader = XmlSession.getSession().getAttribute(Constantes.CURRENT_MAIN_LOADER);
		// 获取请求节点中的系统信息头
		xPathStack.push(Constantes.SYS);
		xPathStack.push(Constantes.INLINE);
		String file = xpath.evaluate("com/@inline", node);
		URI uri = new File(file).toURI();
		AliasNameManager asliasNameManager = AliasNameManager.getSimpleManager();
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, asliasNameManager);
		XmlHeaderParser headerParser = new XmlHeaderParserImpl();
		List<SOPMetadata> headers = headerParser.parse(uri);
		List<SOPMetadata> comheader = mainLoader.comheaders.get(type);
		comheader.addAll(headers);
		int beginPos = file.lastIndexOf("/");
		int endPos = file.lastIndexOf(".");
		mainLoader.comHeadFileName = file.substring(beginPos, endPos);
		
		for (SOPMetadata sopMetadata : headers) {
			mainLoader.getComHeaderLoader().getAliasNameManager().register(sopMetadata.getAlias(), sopMetadata.getName());
		}
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, null);
		xPathStack.poll();
		xPathStack.poll();
	}
	/**
	 * 处理交易数据
	 * @param node
	 * @throws XPathExpressionException 
	 */
	void parseBusis0(PacketType type, Node node) throws XPathExpressionException{
		XPath xpath = XmlSession.getSession().getAttribute(Constantes.XPATH);
		Node busisNode = (Node) xpath.evaluate(Constantes.BUSIS, node, XPathConstants.NODE);
		XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, busisNode);
		busisParser.parse();
	}
}
