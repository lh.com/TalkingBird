/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.impl;

import java.net.URI;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlHeaderParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate.XmlAliasMappingParserDelegate;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate.XmlMetadatasPoolParserDelegate;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate.XmlHeaderParserDelegate;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
/**
 * Xml头信息解析实现<br>
 * 解析过程为：<br>
 * 1.解析别名映射，创建一个别名管理器Constantes.CURRENT_ALIAS_NAME_MANAGER访问，在解析完后不回收<br>
 * 2.解析元信息池，创建一个键值对作为池,使用Constantes.METADATAS_POOL访问，在解析完后回收<br>
 * 3.解析的结果用Constantes.CURRENT_HEADER访问
 * @author liucheng
 *
 */
public class XmlHeaderParserImpl extends AbstractXmlParserImpl implements XmlHeaderParser {
	public static final String TALKINGBIRD_XSD = "header.xsd";
	XmlParser xmlAliasParser = new XmlAliasMappingParserDelegate();
	XmlParser xmlMetadataParser = new XmlMetadatasPoolParserDelegate();
	XmlParser XmlHeaderParser = new XmlHeaderParserDelegate();
	
	public List<SOPMetadata> parse(URI uri) throws XmlParseException {
		try {
			vildateSchema(uri, TALKINGBIRD_XSD);
			Document document = initDoc(uri);
			XPath xpath = initXpath(uri);
			XmlSession.getSession().setAttribute(Constantes.PATH_STACK, new XPathStack()) ;
			XmlSession.getSession().setAttribute(Constantes.DOCUMENT, document);
			XmlSession.getSession().setAttribute(Constantes.XPATH, xpath);
			XmlSession.getSession().setAttribute(Constantes.URI, uri);
			//进行别名映射节点解析
			xmlAliasParser.parse();
			xmlMetadataParser.parse();
			XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, getTrade(document));
			XmlHeaderParser.parse();
		} catch (XmlParseException e) {
			throw e;
		} catch (Exception e) {
			throw new XmlParseException("解析接口文件["+ uri+"]发生异常!",e);
		}
		List<SOPMetadata> headers = XmlSession.getSession().getAttribute(Constantes.CURRENT_HEADER);
		return headers;
	}
	
	Node getTrade(Document document) throws XPathExpressionException{
		XPath xpath = XmlSession.getSession().getAttribute(Constantes.XPATH);
		Node tradenode = (Node) xpath.evaluate(Constantes.INTERFACES, document, XPathConstants.NODE);
		return tradenode;
	}

}
