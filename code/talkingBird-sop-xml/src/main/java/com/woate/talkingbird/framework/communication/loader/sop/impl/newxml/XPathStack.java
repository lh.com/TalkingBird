/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
/**
 * XPath·��ջ
 * @author liucheng
 * 2014-12-25 ����06:01:33
 *
 */
public class XPathStack {
	Deque<String> paths = new LinkedList<String>();
	public String peek(){
		return paths.peekFirst();
	}
	public String poll(){
		return paths.pollFirst();
	}
	public void push(String path){
		paths.push(path);
	}
	@Override
	public String toString() {
		StringBuilder buffer = new StringBuilder();
		Iterator<String> it = paths.descendingIterator();
		while (it.hasNext()) {
			if(buffer.length() != 0){
				buffer.append("/");
			}
			buffer.append(it.next());
		}
		return buffer.toString();
	}
	
}
