/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.impl;

import java.net.URI;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.woate.talkingbird.framework.communication.loader.BootstrapMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlInterfaceParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate.XmlAliasMappingParserDelegate;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate.XmlMainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate.XmlMetadatasPoolParserDelegate;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate.XmlInterfaceParserDelegate;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util.XmlNodeVildateUtil;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.core.AliasNameManager;
/**
 * 接口解析器实现类
 * @author liucheng
 * 
 *
 */
public class XmlInterfaceParserImpl extends AbstractXmlParserImpl implements XmlInterfaceParser {
	public static final String TALKINGBIRD_XSD = "talkingbird.xsd";
	XmlParser xmlAliasParser = new XmlAliasMappingParserDelegate();
	XmlParser xmlMetadataParser = new XmlMetadatasPoolParserDelegate();
	XmlParser xmlInterfaceParser = new XmlInterfaceParserDelegate();
	@Override
	public MainMetadataLoader<SOPMetadata> parse(URI uri) throws XmlParseException {
		//获取会话中的根加载器
		BootstrapMetadataLoader<SOPMetadata> bootstrapLoader = XmlSession.getSession().getAttribute(Constantes.CURRENT_BOOTSTRAP_LOADER);
		try {
			vildateSchema(uri, TALKINGBIRD_XSD);
			Document document = initDoc(uri);
			XPath xpath = initXpath(uri);
			XmlSession.getSession().setAttribute(Constantes.PATH_STACK, new XPathStack()) ;
			XmlSession.getSession().setAttribute(Constantes.DOCUMENT, document);
			XmlSession.getSession().setAttribute(Constantes.XPATH, xpath);
			XmlSession.getSession().setAttribute(Constantes.URI, uri);
			AliasNameManager currentAliasNameManager = AliasNameManager.getSimpleManager();
			XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, currentAliasNameManager);
			//进行别名映射节点解析
			xmlAliasParser.parse();
			xmlMetadataParser.parse();
			Node interfaceNode= getInterface(document);
			String interfaceName = XmlNodeVildateUtil.getValueBySafety(interfaceNode.getAttributes().getNamedItem(Constantes.INTERFACE_NAME));
			XmlMainMetadataLoader mainLoader = new XmlMainMetadataLoader(interfaceName, uri, bootstrapLoader);
			mainLoader.setAliasNameManager(currentAliasNameManager);
			XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, interfaceNode);
			XmlSession.getSession().setAttribute(Constantes.CURRENT_MAIN_LOADER, mainLoader);
			//调用接口处理委托类
			xmlInterfaceParser.parse();
			return mainLoader;
			//创建主加载器
		} catch (XmlParseException e) {
			throw e;
		} catch (Exception e) {
			throw new XmlParseException("解析接口文件["+ uri+"]发生异常!",e);
		}
	}
	
	Node getInterface(Document document) throws XPathExpressionException{
		XPath xpath = XmlSession.getSession().getAttribute(Constantes.XPATH);
		Node interfaceNode = (Node) xpath.evaluate(Constantes.INTERFACES_INTERFACE, document, XPathConstants.NODE);
		return interfaceNode;
	}

}
