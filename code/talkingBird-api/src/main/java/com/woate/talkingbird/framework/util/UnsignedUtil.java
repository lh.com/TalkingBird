/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.util;
/**
 * 将字节中存储的无符号数据转换
 * @author liucheng
 * 2014-12-31 下午01:49:01
 *
 */
public class UnsignedUtil {
	/**
	 * 将无符号字节数转换为整数
	 * @param bb 无符号字节数
	 * @return 整数
	 */
	public static int unsignedByte2int(byte bb) {
		return bb & 0xFF;
	}
	/**
	 * 将整数转换为无符号字节数
	 * @param value 整数
	 * @return 无符号字节数
	 */
	public static byte int2UnsignedByte(int value) {
		return (byte) (value & 0xFF);
	}
	/**
	 * 将无符号short转换为整数
	 * @param bb 无符号short
	 * @return 整数
	 */
	public static int unsignedShort2int(short bb) {
		return bb & 0xFFFF;
	}
	/**
	 * 将整数转换为无符号short
	 * @param value 整数
	 * @return 无符号short
	 */
	public static short int2UnsignedShort(int value) {
		return (short) (value & 0xFFFF);
	}
}
