package com.woate.talkingbird.framework.util;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by woate on 16-4-30.
 */
public class Resources {
    public static final String CLASSPATH_ALL_URL_PREFIX = "classpath*:";
    public static final String CLASSPTH_URL_PREFIX  = "classpath:";
    String CONFIG_LOCATION_DELIMITERS = ",; \t\n";
    public String[] tokenizeToStringArray(String str, String delimiters, boolean trimTokens, boolean ignoreEmptyTokens) {
        if (str == null) {
            return null;
        }
        StringTokenizer st = new StringTokenizer(str, delimiters);
        List<String> tokens = new ArrayList<String>();
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            if (trimTokens) {
                token = token.trim();
            }
            if (!ignoreEmptyTokens || token.length() > 0) {
                tokens.add(token);
            }
        }
        return tokens.toArray(new String[tokens.size()]);
    }
    /**
     * classpath:test1/demo.txt
     * classpath:test1/demo.txt;classpath:test1/demo.txt
     * classpath*:test1/demo.txt
     * classpath*:test1/demo.txt;classpath*:test1/demo.txt
     * @param packagePath 路径字符串
     * @return 路径数组t
     */
    public URI[] find(String packagePath){
        List<URI> pathList = new ArrayList<URI>();
        String[] packagePaths = tokenizeToStringArray(packagePath,CONFIG_LOCATION_DELIMITERS,true, true );
        for (String packagePath0 : packagePaths){
            if(packagePath0.startsWith(CLASSPTH_URL_PREFIX)){
                packagePath0 = packagePath0.substring(CLASSPTH_URL_PREFIX.length());
                //将包路径转换为文件路径
                String filePath = ClassUtil.convertClassNameToResourcePath(packagePath0);
                URL url = this.getClass().getResource(filePath);
                if(url == null){
                    System.err.println("未发现["+ packagePath0 +"]");
                }else{
                    try {
                        pathList.add(url.toURI());
                    } catch (URISyntaxException e) {
                        System.err.println("发生错误["+ packagePath0 +"]");
                    }
                }
            }else if(packagePath0.startsWith(CLASSPATH_ALL_URL_PREFIX)){
                //TODO
            }else{
                String filePath = ClassUtil.convertClassNameToResourcePath(packagePath0);
                File file = new File(filePath);
                if(file.exists() && file.isFile()){
                    pathList.add(file.toURI());
                }else{
                    System.err.println("未发现["+ packagePath0 +"]");
                }
            }
        }
        return pathList.toArray(new URI[pathList.size()]);
    }
}
