/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.core.converter.impl;

import java.util.Date;
import java.util.Map;

import com.woate.talkingbird.framework.util.DateUtil;

/**
 * 把字节数组转换为日期类型<br>
 * 
 * @author liucheng 2014-12-4 上午09:29:04
 * 
 */
public class ByteArray2DateConverter extends AbstractByteArrayConverter {
	@Override
	public String getInfo() {
		return "com.brilliance.framework.communication.converter.impl.ByteArray2DateConverter/1.0";
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convert(Object in, Map<String, Object> params) {
		super.vlidate(in, params);
		byte[] bytes = (byte[]) in;
		String msg = new String(bytes).trim();
		if(DateUtil.getPartten(msg) == null){
			throw new IllegalArgumentException("转换器输入日期格式不支持!");
		}
		Date date = DateUtil.toDate(msg);
		return (T)date;
	}
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
