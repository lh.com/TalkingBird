/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.core.converter.impl;

import java.nio.charset.Charset;
import java.util.Map;

/**
 * 将字符串转换为字节数组
 * @author liucheng
 * 2014-12-5 上午11:24:52
 *
 */
public class String2ByteArrayConverter extends AbstractConverter {

	
	@Override
	public String getInfo() {
		return "com.brilliance.framework.communication.converter.impl.String2ByteArrayConverter/1.0";
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convert(Object in, Map<String, Object> params) {
		vlidate(in, params);
		if(!(in instanceof String)){
			throw new IllegalArgumentException("转换器输入数据["+ in +"]需要是[String],实际是["+in.getClass()+"]");
		}
		String encoding = (String)params.get(INPUT_ENCODING);
		Charset charset = Charset.forName(encoding);
		String msg = (String)in;
		return (T) msg.getBytes(charset);
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
