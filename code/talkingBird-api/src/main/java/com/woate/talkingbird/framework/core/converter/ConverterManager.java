package com.woate.talkingbird.framework.core.converter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.woate.talkingbird.framework.core.converter.impl.BigDecimal2ByteArrayConverter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2BigDecimalConverter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2DateConverter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2IntegerConverter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.Date2ByteArrayConverter;
import com.woate.talkingbird.framework.core.converter.impl.Date2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.Integer2ByteArrayConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
/**
 * 转换器管理器
 * @author liucheng
 * 2014-12-30 上午10:03:27
 *
 */
@SuppressWarnings("rawtypes")
public class ConverterManager {
	static final Map<Class, Map<Class, Converter>> sourceTargetMap = new HashMap<Class, Map<Class,Converter>>();
	static{
		//注册源为字符串
		Map<Class, Converter> stringMap = new HashMap<Class, Converter>();
		stringMap.put(byte[].class, new String2ByteArrayConverter());
		sourceTargetMap.put(String.class, stringMap);
		//注册源为Date
		Map<Class, Converter> dateMap = new HashMap<Class, Converter>();
		dateMap.put(String.class, new Date2StringConverter());
		dateMap.put(byte[].class, new Date2ByteArrayConverter());
		sourceTargetMap.put(Date.class, dateMap);
		//注册源为BigDecimal
		Map<Class, Converter> bigdecimalMap = new HashMap<Class, Converter>();
		bigdecimalMap.put(byte[].class, new BigDecimal2ByteArrayConverter());
		sourceTargetMap.put(BigDecimal.class, bigdecimalMap);
		//注册源为Integer
		Map<Class, Converter> integerMap = new HashMap<Class, Converter>();
		integerMap.put(byte[].class, new Integer2ByteArrayConverter());
		sourceTargetMap.put(Integer.class, integerMap);
		sourceTargetMap.put(Integer.TYPE, integerMap);
		//注册源为byte[]
		Map<Class, Converter> byteArrayMap = new HashMap<Class, Converter>();
		byteArrayMap.put(String.class, new ByteArray2StringConverter());
		byteArrayMap.put(Integer.class, new ByteArray2IntegerConverter());
		byteArrayMap.put(Integer.TYPE, new ByteArray2IntegerConverter());
		byteArrayMap.put(Date.class, new ByteArray2DateConverter());
		byteArrayMap.put(BigDecimal.class, new ByteArray2BigDecimalConverter());
		sourceTargetMap.put(byte[].class, byteArrayMap);
	}
	
	public static Converter find(Class source, Class target){
		Map<Class, Converter> map = sourceTargetMap.get(source);
		Converter converter = map.get(target);
		return converter;
	}
}
