/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.metadata;
/**
 * 报文元信息接口，提供了改变元信息描述信息的能力<br>
 * @author woate
 * 2014-11-24 下午01:37:10
 *
 */
public interface MetadataWriteable extends Metadata{
	/**
	 * 设置元信息名称
	 * @param name 名称
	 */
	public void setName(String name);
}
