package com.woate.talkingbird.framework.util;

/**
 * Created by woate on 16-4-30.
 */
public abstract class ClassUtil {
    private static final char PACKAGE_SEPARATOR = '.';
    private static final char PATH_SEPARATOR = '/';

    public static String convertClassNameToResourcePath(String className) {
        if(className == null) {
            throw new IllegalArgumentException("Class name must not be null");
        }
        int lastDot = className.lastIndexOf('.');
        if(lastDot > -1){
            char[] chars = className.replace(PACKAGE_SEPARATOR, PATH_SEPARATOR).toCharArray();
            chars[lastDot] = '.';
            return new String(chars);
        }else{
            return null;
        }

    }
}
