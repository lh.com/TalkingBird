/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader;

import java.util.Collection;

import com.woate.talkingbird.framework.core.AliasNameManager;

/**
 * 根加载器<br>
 * 加载器加载顺序<br>
 * BootstrapMetadataLoader用于实现加载方式<br>
 * MainMetadataLoader用于管理单个交易（单个交易或者组合交易）<br>
 * MetadataLoader用于管理元信息<br>
 * @author liucheng
 * 2014-12-22 下午02:40:39
 *
 */
public interface BootstrapMetadataLoader<T> extends Loader{
	/**
	 * 加载根加载器
	 * @return 加载器
	 */
	BootstrapMetadataLoader<T> load();
	/**
	 * 根据名称获取主加载器
	 * @param name 主加载器的名称
	 * @return 加载器
	 */
	MainMetadataLoader<T> getLoader(String name);
	/**
	 * 获取接口名列表
	 * @return 接口名列表
	 */
	Collection<String> getInterfaceNames();
	/**
	 * 获取别名管理器
	 * @return 别名管理器
	 */
	AliasNameManager getAliasNameManager();
	
}
