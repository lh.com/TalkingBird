/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.core.converter.impl;

import java.util.Map;

import com.woate.talkingbird.framework.core.converter.Converter;
/**
 * 转换器抽象类
 * @author liucheng
 * 2014-12-4 上午09:32:26
 *
 */
public abstract class AbstractConverter implements Converter{
	protected void vlidate(Object in, Map<String, Object> params){
		if(in == null){
			throw new NullPointerException("转换器输入为null");
		}
		if(params == null){
			throw new NullPointerException("转换器参数params为null");
		}
	}
}
