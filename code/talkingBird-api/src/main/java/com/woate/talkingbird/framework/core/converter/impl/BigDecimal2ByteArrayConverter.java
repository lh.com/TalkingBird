/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.core.converter.impl;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 将BigDecimal转换为字节数组
 * @author liucheng
 * 2014-12-5 上午11:24:52
 *
 */
public class BigDecimal2ByteArrayConverter extends AbstractConverter {

	@Override
	public String getInfo() {
		return "com.brilliance.framework.communication.converter.impl.BigDecimal2ByteArrayConverter/1.0";
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convert(Object in, Map<String, Object> params) {
		vlidate(in, params);
		if(!(in instanceof BigDecimal)){
			throw new IllegalArgumentException("转换器输入数据不是BigDecimal");
		}
		BigDecimal bd = (BigDecimal)in;
		String msg = bd.toString();
		return (T) msg.getBytes();
	}
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
