/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader;

import java.util.List;

import com.woate.talkingbird.framework.core.AliasNameManager;
/**
 * SOP描述信息加载
 * @author woate
 * 2014-12-8 下午04:32:50
 *
 */
public interface MetadataLoader<T> extends Loader{
	/**
	 * 获取加载器名字，实际是头文件的文件名
	 * @return 头文件的文件名
	 */
	String getHeadFileName();
	/**
	 * 加载SOP元信息
	 * @return 元信息列表
	 */
	MetadataLoader<T> load();
	/**
	 * 获取加载后的元信息列表，顺序按照定义顺序
	 * @param type 包类型
	 * @return 元信息列表
	 */
	List<T> listHeader(PacketType type);
	
	/**
	 * 获取别名管理器
	 * @return 别名管理器
	 */
	AliasNameManager getAliasNameManager();
	/**
	 * 获取主加载器
	 * @return 主加载器
	 */
	MainMetadataLoader<T> getMainLoader();
}
