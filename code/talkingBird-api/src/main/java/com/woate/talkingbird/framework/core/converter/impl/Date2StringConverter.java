/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.core.converter.impl;

import java.util.Date;
import java.util.Map;

import com.woate.talkingbird.framework.util.DateUtil;
/**
 * 日期转换为字符串<br>
 * 输出的格式支持<br>
 * yyyy-MM-dd<br>
 * yyyy/MM/dd<br>
 * yyyyMMdd<br>
 * yyyyMMddHHmmss<br>
 * HHmmss<br>
 * HH:mm:ss<br>
 * 其他日期格式不支持
 * @author liucheng
 * 2014-12-5 上午08:46:32
 *
 */
public class Date2StringConverter extends AbstractConverter {


	@Override
	public String getInfo() {
		return "com.brilliance.framework.communication.converter.impl.Date2StringConverter/1.0";
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convert(Object in, Map<String, Object> params) {
		vlidate(in, params);
		Date date = (Date) in;
		String pattern = (String) params.get(DATE_FORMAT);
		if (pattern == null) {
			throw new NullPointerException("转换器日期格式未设置");
		}
		if (!pattern.equals(DateUtil.DAY_FORMAT1) 
				&& !pattern.equals(DateUtil.DAY_FORMAT2) 
				&& !pattern.equals(DateUtil.FILE_FORMAT2)
				&& !pattern.equals(DateUtil.FILE_FORMAT3)
				&& !pattern.equals(DateUtil.FILE_FORMAT4)
				&& !pattern.equals(DateUtil.FILE_FORMAT5)) {
			throw new IllegalArgumentException("转换器输出日期格式不支持");
		}
		String msg = DateUtil.toString(date, pattern);
		return (T)msg;
	}
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
