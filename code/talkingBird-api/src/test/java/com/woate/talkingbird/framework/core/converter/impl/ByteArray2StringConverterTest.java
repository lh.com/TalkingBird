package com.woate.talkingbird.framework.core.converter.impl;

import java.util.HashMap;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;

public class ByteArray2StringConverterTest {
	@Test(expected=IllegalArgumentException.class)
	public void testNoInputEncodingConvert(){
		Converter c = new ByteArray2StringConverter();
		c.convert(new byte[10], new HashMap<String, Object>());
	}
	@Test(expected=NullPointerException.class)
	public void testNullParamsConvert(){
		Converter c = new ByteArray2StringConverter();
		c.convert(new byte[10], null);
	}
	@Test(expected=NullPointerException.class)
	public void testNullInConvert(){
		Converter c = new ByteArray2StringConverter();
		c.convert(null, new HashMap<String, Object>());
	}
	@Test
	public void testConvert1(){
		Converter c = new ByteArray2StringConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(ByteArray2StringConverter.INPUT_ENCODING, "UTF-8");
		Object obj = c.convert(new byte[]{(byte)0xE4, (byte)0xB8, (byte)0x8A, (byte)0xE5, (byte)0xB8, (byte)0x9D, (byte)0xE5, (byte)0x95, (byte)0x8A}, params);
		System.out.println(obj);
		Assert.assertEquals("�ϵ۰�", obj);
	}
}
