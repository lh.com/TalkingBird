package com.woate.talkingbird.framework.core.converter.impl;

import java.util.HashMap;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2IntegerConverter;

public class ByteArray2IntegerConverterTest {
	@Test
	public void testNoInputEncodingConvert(){
		Converter c = new ByteArray2IntegerConverter();
		c.convert(new byte[10], new HashMap<String, Object>());
	}
	@Test(expected=NullPointerException.class)
	public void testNullParamsConvert(){
		Converter c = new ByteArray2IntegerConverter();
		c.convert(new byte[10], null);
	}
	@Test(expected=NullPointerException.class)
	public void testNullInConvert(){
		Converter c = new ByteArray2IntegerConverter();
		c.convert(null, new HashMap<String, Object>());
	}
	@Test
	public void testConvert1(){
		Converter c = new ByteArray2IntegerConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		Object obj = c.convert(new byte[]{(byte)0x00 , (byte)0x32}, params);
		System.out.println(obj);
		Assert.assertEquals(50, obj);
	}
	@Test
	public void testConvert2(){
		Converter c = new ByteArray2IntegerConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		byte[] bytes = new byte[]{(byte)0x01 , (byte)0x1F};
		Object obj = c.convert(bytes, params);
		System.out.println(obj);
		Assert.assertEquals(287, obj);
//		short byte0 = (short) ((255 + 256) / 256 & 0xff);
//		short byte1 = (short) ((255 + 256) & 0xff);
//		System.out.println(byte0);
//		System.out.println(byte1);
		System.out.println(Byte.MAX_VALUE);
	}
	@Test
	public void testConvert3(){
		Converter c = new ByteArray2IntegerConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		byte[] bytes = new byte[]{(byte)0x00 , -37};
		Object obj = c.convert(bytes, params);
		System.out.println(obj);
		Assert.assertEquals(0xDB, obj);
	}
	@Test
	public void testConvert4(){
		Converter c = new ByteArray2IntegerConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		byte[] bytes = new byte[]{(byte)0x01 , -37};
		Object obj = c.convert(bytes, params);
		System.out.println(obj);
		Assert.assertEquals(0x1DB, obj);
	}
}
