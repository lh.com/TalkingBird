package com.woate.talkingbird.framework.core.converter.impl;

import java.util.Arrays;
import java.util.HashMap;


import org.junit.Test;

import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.core.converter.impl.Integer2ByteArrayConverter;

public class Integer2ByteArrayConverterTest {
	@Test
	public void testConvert2(){
		Converter c = new Integer2ByteArrayConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		byte[] obj = c.convert(5, params);
		System.out.println(Arrays.toString(obj));
	}
}
