package com.woate.talkingbird.framework.core.impl;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.core.AliasNameException;
import com.woate.talkingbird.framework.core.AliasNameManager;

public class AliasNameManagerByMemorySimpleImplTest {
	@Test(expected=AliasNameException.class)
	public void testRegister1(){
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register(null, null);
	}
	@Test(expected=AliasNameException.class)
	public void testRegister2(){
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register(null, "A");
	}
	@Test(expected=AliasNameException.class)
	public void testRegister3(){
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register(null, "");
	}
	@Test(expected=AliasNameException.class)
	public void testRegister4(){
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register("", null);
	}
	@Test(expected=AliasNameException.class)
	public void testRegister5(){
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register("", "A");
	}
	@Test(expected=AliasNameException.class)
	public void testRegister6(){
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register("", "");
	}
	public void testRegister7(){
		String A = "A";
		String B = "B";
		String a = "a";
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register(A, a);
		mgr.register(B, a);
	}
	
	/**
	 * 测试只有一个别名的映射
	 */
	@Test
	public void testFindAliasName1(){
		String A = "A";
		String a = "a";
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register(A, a);
		String real =mgr.findRealName(A);
		Assert.assertEquals(a, real);
	}
	/**
	 * 测试多个别名映射同一个真实名称
	 */
	@Test
	public void testFindAliasName2(){
		String A = "A";
		String B = "B";
		String a = "a";
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register(A, a);
		mgr.register(B, a);
		String real1 =mgr.findRealName(A);
		Assert.assertEquals(a, real1);
		String real2 =mgr.findRealName(B);
		Assert.assertEquals(a, real2);
	}
	/**
	 * 测试多个别名映射同一个真实名称
	 */
	@Test(expected=AliasNameException.class)
	public void testFindAliasName3(){
		String A = "A";
		String b = "b";
		String a = "a";
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register(A, a);
		mgr.register(A, b);
	}
	/**
	 * 测试在多个别名注册到一个真实名称时，移除的测试
	 */
	@Test
	public void testRemove1(){
		String A = "A";
		String B = "B";
		String a = "a";
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register(A, a);
		mgr.remove(A);
		Assert.assertNull(mgr.findRealName(A));
		Assert.assertTrue(mgr.findAliasName(a).isEmpty());
		mgr.register(A, a);
		mgr.register(B, a);
		mgr.remove(A);
		Assert.assertNull(mgr.findRealName(A));
		Assert.assertEquals(a, mgr.findRealName(B));
		Assert.assertTrue(!mgr.findAliasName(a).isEmpty());
		mgr.remove(B);
		Assert.assertNull(mgr.findRealName(A));
		Assert.assertNull(mgr.findRealName(B));
		Assert.assertTrue(mgr.findAliasName(a).isEmpty());
	}
	@Test
	public void testRemove2(){
		String A = "A";
		String B = "B";
		String a = "a";
		String b = "b";
		AliasNameManager mgr = AliasNameManager.getSimpleManager();
		mgr.register(A, a);
		mgr.register(B, b);
		mgr.remove(A);
		Assert.assertNull(mgr.findRealName(A));
		Assert.assertEquals(b, mgr.findRealName(B));
		Assert.assertTrue(mgr.findAliasName(a).isEmpty());
		Assert.assertTrue(!mgr.findAliasName(b).isEmpty());
		mgr.register(A, a);
		Assert.assertEquals(a, mgr.findRealName(A));
		Assert.assertEquals(b, mgr.findRealName(B));
		Assert.assertTrue(!mgr.findAliasName(a).isEmpty());
		Assert.assertTrue(!mgr.findAliasName(b).isEmpty());
	}
}
