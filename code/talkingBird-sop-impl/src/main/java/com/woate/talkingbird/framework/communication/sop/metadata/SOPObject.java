/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata;

import java.util.List;


/**
 * SOP对象接口<br>
 * 其子接口有Bean、Form<br>
 * Bean是对单条对象的描述，可以理解为一个具名的DTO类，在DTO内由字段构成。<br>
 * Form是对多条对象的描述，可以理解为一个具名的DTO List，在DTO内由字段构成。<br>
 * @author liucheng 2014-11-24 下午05:13:25
 * 
 */
public interface SOPObject extends SOPMetadata{
	/**
	 * 获取SOP报文的包装类型
	 * @return
	 */
	Class<?> getWrapClass();
	/**
	 * 按字段名获取字段描述元信息
	 * @param name 字段名
	 * @return 元信息
	 */
	SOPVariableField getField(String name);
	/**
	 * 获取字段描述元信息列表<br>
	 * 使用该方法可以获取SOP报文对象中的字段元信息列表，无论是Bean还是Form
	 * @return 元信息列表
	 */
	public List<SOPVariableField> getFields();
}
