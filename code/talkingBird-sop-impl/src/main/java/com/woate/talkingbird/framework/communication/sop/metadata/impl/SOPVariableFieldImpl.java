/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
import com.woate.talkingbird.framework.communication.sop.util.SOPUtil;
import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.util.Hex;

/**
 * 不定长字段对象实现
 * 
 * @author liucheng 2014-11-27 上午09:18:17
 * 
 */
public class SOPVariableFieldImpl extends AbstractSOPFieldImpl implements SOPVariableFieldWriteable {
	/**
	 * 最大长度
	 */
	protected int maxLength;
	/**
	 * 最小长度
	 */
	protected int minLength;

	SOPVariableFieldImpl() {
		super();
	}
	
	public void vildate(){
		super.vildate();
		if (minLength > maxLength) {
			throw new IllegalArgumentException("元信息字段[" + name + "]["+ desc+"]描述的最小长度大于最大长度");
		}
	}

	@Override
	public int getMaxLength() {
		return maxLength;
	}

	@Override
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	@Override
	public int getMinLength() {
		return minLength;
	}

	@Override
	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	@Override
	public byte[] format(Object input) throws TransformException {
		//如果字段为常量模式,则使用常量值
		if(valueMode == ValueMode.CONSTANT){
			input = value;
		}
		//如果字段为默认模式,同时输入为null,则使用默认值
		if(valueMode == ValueMode.DEFAULT && input == null){
			input = value;
			SOPSession.getSession().setAttribute(name, value);
		}
		byte[] msgs = null;
		//这种情况说明是个占位的
		if(this.maxLength == 0){
			return new byte[]{(byte)0};
		}
		//如果允许为null,自动转换为占位
		if(input==null && allowNull){
			return new byte[]{(byte)0};
		}
		//检查输入是为空
		if (input == null) {
			throw new TransformException("由于字段[" + name + "]["+ desc+"]输入数据为null,导致无法处理");
		}
		if (formatConverter == null) {
			if (input instanceof byte[]) {
				msgs = (byte[]) input;
			} else {
				throw new TransformException("字段[" + name + "]["+ desc+"]由于字段数据不是byte[],同时未配置转换器(converter),导致无法处理[" + input + "]");
			}
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			if(clazz == String.class){
				if(encoding == null || encoding.isEmpty()){
					map.put(Converter.INPUT_ENCODING, "UTF-8");
				}else{				
					map.put(Converter.INPUT_ENCODING, encoding);
				}
			}else if(clazz == Date.class){
				map.put(Converter.DATE_FORMAT, "yyyyMMdd");
			}
			try {
				msgs = formatConverter.convert(input, map);				
			} catch (Exception e) {
				throw new TransformException("字段[" + name + "]["+ desc+"]转换时发生异常", e);
			}
		}
		int infactLen = msgs.length;
		if (infactLen < minLength || infactLen > maxLength) {
			if(minLength == maxLength){
				throw new TransformException("字段[" + name + "]["+ desc+"]实际长度[" + infactLen + "]不等于元信息定义的[" + minLength + "]！");				
			}else{				
				throw new TransformException("字段[" + name + "]["+ desc+"]实际长度[" + infactLen + "]不在元信息定义的[" + minLength + "," + maxLength + "]区间范围内！");
			}
		}
		byte[] sopBytes = SOPUtil.sopWrap(msgs);
		return sopBytes;
	}

	/**
	 * 根据SOP报文的规定，只能存放0~0xFA的长度，0xFF作为续行符
	 */
	@Override
	public Object parse(ByteBuffer input) throws TransformException {
		int maxDataLen = input.remaining();
		// 分配来用于存储无续行符的字节数组
		ByteBuffer data = ByteBuffer.allocate(maxDataLen);
		// 读取第一个长度
		int infactLen = 0;
		int count = 0;
		boolean readfinish = false;
		while (infactLen + count < maxDataLen && !readfinish) {
			int readLen = input.get() & CONTINUANCE;
			count++;
			// 如果实际长度不等于元信息定义的长度
			switch (readLen) {
			case CONTROL1:
				break;
			case CONTROL2:
				break;
			case CONTROL3:
				break;
			case CONTROL4:
				break;
			case CONTINUANCE:
				// 如果续行符则增加0xFA
				infactLen += MAX_LENGTH;
				// 读取的是
				byte[] bytes0 = new byte[MAX_LENGTH];
				input.get(bytes0);
				data.put(bytes0);
				continue;
			default:
				// 如果是长度，则增加读取的长度
				infactLen += readLen;
				// 读取的是长度
				byte[] bytes1 = new byte[readLen];
				input.get(bytes1);
				data.put(bytes1);
				readfinish = true;
				break;
			}
		}
		if (infactLen < minLength || infactLen > maxLength) {
			throw new TransformException("字段[" + name + "]["+ desc+"]实际长度[" + infactLen + "]不在[" + minLength + "," + maxLength + "]区间里！");
		}
		data.rewind();
		byte[] newbytes = new byte[infactLen];
		data.get(newbytes);
		Object val = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			if(clazz == String.class){
				if(encoding == null || encoding.isEmpty()){
					map.put(Converter.INPUT_ENCODING, "UTF-8");
				}else{				
					map.put(Converter.INPUT_ENCODING, encoding);
				}
			}else if(clazz == Date.class){
				map.put(Converter.DATE_FORMAT, "yyyyMMdd");
			}
			val = parseConverter.convert(newbytes, map);
		} catch (Exception e) {
			throw new TransformException("字段[" + name + "]["+ desc+"]转换时发生异常,["+ Hex.toHexString(newbytes) +"]", e);
		}
		return val;
	}

	@Override
	public int getMaxSOPLength() {
		double maxLen = (double)getMaxLength();
		int times = (int)Math.ceil( maxLen/ (double)0xFA);
		return times + getMaxLength();
	}

	@Override
	public int getMinSOPLength() {
		return 0;
	}



}
