/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.interp.header;

import java.nio.ByteBuffer;

import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.sop.SOPFormatter;
import com.woate.talkingbird.framework.communication.sop.SOPParser;
import com.woate.talkingbird.framework.communication.sop.interp.SOPEntity;
import com.woate.talkingbird.framework.communication.sop.interp.header.delegate.BusiHeaderFormatterDelegate;
import com.woate.talkingbird.framework.communication.sop.interp.header.delegate.BusiHeaderParserDelegate;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
/**
 * 交易数据头
 * @author liucheng
 *
 */
public class SOPBusiHeader implements SOPEntity{
	SOPFormatter formatter;
	SOPParser parser;
	String tradeCode;
	
	public SOPBusiHeader(String tradeCode, BusiDataMetadateLoader<SOPMetadata> loader) {
		this.tradeCode = tradeCode;
		formatter = new BusiHeaderFormatterDelegate(loader);
		parser = new BusiHeaderParserDelegate(loader);
	}
	@Override
	public ByteBuffer format() throws TransformException {
		return formatter.format();
	}

	@Override
	public void parse(ByteBuffer input) throws TransformException {
		parser.parse(input);
	}

}
