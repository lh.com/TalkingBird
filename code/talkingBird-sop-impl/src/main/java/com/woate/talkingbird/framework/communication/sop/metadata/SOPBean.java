/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata;

import java.nio.ByteBuffer;

import com.woate.talkingbird.framework.communication.Formatter;
import com.woate.talkingbird.framework.communication.Parser;
/**
 * SOP单条对象接口<br>
 * 一个单条对象含有若干个具名字段对象<br>
 * @author liucheng
 * 2014-12-16 下午02:12:46
 *
 */
public interface SOPBean extends SOPObject, Formatter<Object, byte[]>, Parser<ByteBuffer, Object> {
	/**
	 * 获取最大的SOP报文长度<br>
	 * 区别于getMaxLength();
	 * @return
	 */
	public int getMaxSOPLength();
	/**
	 * 获取最小的SOP报文长度<br>
	 * 区别于getMinLength();
	 * @return
	 */
	public int getMinSOPLength();
}
