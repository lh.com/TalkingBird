/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPFormWriteable;

/**
 * SOP多条对象实现
 * 
 * @author liucheng 2014-11-24 下午05:13:12
 * 
 */
public class SOPFormImpl<T> extends AbstractSOPObjectImpl implements SOPFormWriteable<T> {
	public SOPFormImpl(String name, String alias, String desc, String encoding, Class<?> wrapClass) {
		super(name, alias, desc, encoding, wrapClass);
	}

	@Override
	public byte[] format(List<T> input) throws TransformException {
		if (input == null) {
			throw new TransformException("元信息[" + name + "][" + desc + "]描述的输入为空指针!");
		}
		if (!input.isEmpty() && wrapClass != input.get(0).getClass()) {
			throw new TransformException("元信息[" + name + "][" + desc + "]描述的包装类[" + wrapClass.getName() + "]和实际数据包装类[" + input.get(0).getClass().getName() + "]不一致");
		}
		if (input.size() > MAX_RECORDS) {
			throw new IllegalArgumentException("元信息[" + name + "][" + desc + "]目前SOP报文记录条数最大为100");
		}
		init();
		// 分配的缓冲区长度应该按照SOP报文格式确定
		ByteBuffer buffer = ByteBuffer.allocate(getMaxSOPLength(input.size()));
		buffer.put((byte) name.length());
		buffer.put(formatName());
		buffer.put((byte) input.size());
		buffer.put((byte) fields.size());
		for (Object obj : input) {
			format0(obj, buffer);
		}
		int len = buffer.position();
		buffer.flip();
		byte[] newbytes = new byte[len];
		buffer.get(newbytes);
		return newbytes;
	}

	void format0(Object obj, ByteBuffer buffer) throws TransformException {
		Map<String, Object> map = null;
		Class[] interfaces = wrapClass.getInterfaces();
		//如果输入类是Map接口,则使用Map访问
		for (Class interfaceClazz : interfaces) {
			if (interfaceClazz.isAssignableFrom(Map.class)) {
				map = (Map) obj;
				break;
			}			
		}
		for (SOPVariableField field : fields) {
			Object val = null;
			if (map != null) {
				val = map.get(field.getName());
			} else {
				Method m = beanMethodCache.get(field.getName() + "@getter");
				if (m == null) {
					throw new TransformException("不存在元信息[" + name + "][" + desc + "]描述的JavaBean[" + field.getName() + "]getter！");
				}
				try {
					val = m.invoke(obj, new Object[0]);
				} catch (Exception e) {
					throw new TransformException("元信息[" + name + "][" + desc + "]描述的JavaBean[" + field.getName() + "]getter执行时发生异常！");
				}
			}
			byte[] bytes = field.format(val);
			buffer.put(bytes);
		}
	}

	@Override
	public List<T> parse(ByteBuffer input) throws TransformException {
		if (input == null) {
			throw new TransformException("元信息[" + name + "][" + desc + "]描述的输入报文不能为空指针!");
		}
		if (input.limit() < getMinLength()) {
			throw new TransformException("元信息[" + name + "][" + desc + "]描述的输入报文必须大于最小报文长度!");
		}
		init();
		int infactlen = input.get();
		if(infactlen != name.getBytes().length){
			throw new TransformException("元信息[" + name + "][" + desc + "]长度["+name.getBytes().length+"]与实际长度["+ infactlen+"]不一致!");
		}
		byte[] namebytes = new byte[infactlen];
		input.get(namebytes);
		int size = input.get();
		int colmon = input.get();
		if (colmon != fields.size()) {
			throw new TransformException("元信息[" + name + "][" + desc + "]描述表单栏位数与实际报文不一致");
		}
		List<T> list = new ArrayList<T>();
		for (int i = 0; i < size; i++) {
			T obj = parse0(input);
			list.add(obj);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	T parse0(ByteBuffer input) throws TransformException {
		Object wrapper = null;
		try {
			wrapper = wrapClass.newInstance();
		} catch (Exception e) {
			throw new TransformException("元信息[" + name + "][" + desc + "]描述表单包装类[" + wrapClass.getName() + "]创建实例时发生异常！");
		}
		Map<String, Object> map = null;
		Class[] interfaces = wrapClass.getInterfaces();
		//如果输入类是Map接口,则使用Map访问
		for (Class interfaceClazz : interfaces) {
			if (interfaceClazz.isAssignableFrom(Map.class)) {
				map = (Map) wrapper;
				break;
			}			
		}
		for (SOPVariableField field : fields) {
			Object val = field.parse(input);
			if (map != null) {
				map.put(field.getName(), val);
			} else {
				Method m = beanMethodCache.get(field.getName() + "@setter");
				if (m == null) {
					throw new TransformException("不存在元信息[" + name + "][" + desc + "]描述的JavaBean[" + field.getName() + "]setter！");
				}
				try {
					m.invoke(wrapper, val);
				} catch (Exception e) {
					throw new TransformException("元信息[" + name + "][" + desc + "]描述的JavaBean[" + field.getName() + "]setter执行时发生异常！");
				}
			}
		}
		return (T) wrapper;
	}

	@Override
	public int getMinLength() {
		return 0;
	}

	@Override
	public int getMaxSOPLength(int size) {
		int len = 0;
		for (SOPVariableField field : fields) {
			len += field.getMaxSOPLength();
		}
		return getMinSOPLength(size) + len * size;
	}

	@Override
	public int getMinSOPLength(int size) {
		return 1 + name.length() + 1 + 1 + getMinLength();
	}

	@Override
	public String toString() {
		return super.toString();
	}

}
