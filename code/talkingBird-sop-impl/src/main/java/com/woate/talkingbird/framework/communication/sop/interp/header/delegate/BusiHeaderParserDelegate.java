/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.interp.header.delegate;

import java.nio.ByteBuffer;
import java.util.List;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPParser;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
/**
 * 业务信息头反编排委托类<br>
 * @author liucheng
 * 2014-12-16 下午03:19:23
 *
 */
public class BusiHeaderParserDelegate implements SOPParser{
	/**
	 * 元信息加载器
	 */
	BusiDataMetadateLoader<SOPMetadata> loader;
	public BusiHeaderParserDelegate(BusiDataMetadateLoader<SOPMetadata> loader) {
		this.loader = loader;
	}

	@Override
	public void parse(ByteBuffer input) throws TransformException {
		SOPSession session = SOPSession.getSession();
		PacketType packetType = session.getAttribute(Dictionary.PACKET_TYPE);
		List<SOPMetadata> fields = this.loader.listHeader(packetType);
		SOPMetadata fieldMetadata = null;
		if(session.busiDatas().isEmpty()){
			throw new TransformException("交易["+ loader.getTradeCode()+ "]未提供交易数据");
		}
		//获取队列顶部数据
		SOPBusiWrapper busiWrapper = session.peekLastBusiData();
		for (int i = 0; i < fields.size(); i++) {
			fieldMetadata = fields.get(i);
			Object value = parse0((SOPVariableField)fieldMetadata, input);
			busiWrapper.setAttribute(fieldMetadata.getName(), value);
		}
	}
	
	Object parse0(SOPVariableField fieldMetadata,ByteBuffer input) throws TransformException{
		return fieldMetadata.parse(input);
	}

}
