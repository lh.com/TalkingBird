/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata;

import java.nio.ByteBuffer;
import java.util.List;

import com.woate.talkingbird.framework.communication.Formatter;
import com.woate.talkingbird.framework.communication.Parser;


/**
 * SOP多条对象接口<br>
 * 一个多条对象含有若干个具名字段对象<br>
 * @author liucheng
 * 2014-11-24 下午05:13:12
 *
 */
public interface SOPForm<T> extends SOPObject,Formatter<List<T>, byte[]>, Parser<ByteBuffer, List<T>>{
	/**
	 * 最大记录数<br>
	 * 一个多条对象支持的最大条数,如果超过该值只能使用文件方式进行批量通信<br>
	 */
	public final int MAX_RECORDS = 100;
	
	/**
	 * 获取最大的SOP报文长度<br>
	 * 区别于getMaxLength();
	 * @param size 运行时条数
	 * @return
	 */
	public int getMaxSOPLength(int size);
	/**
	 * 获取最小的SOP报文长度<br>
	 * 区别于getMinLength();
	 * @param size 运行时条数
	 * @return
	 */
	public int getMinSOPLength(int size);
}
