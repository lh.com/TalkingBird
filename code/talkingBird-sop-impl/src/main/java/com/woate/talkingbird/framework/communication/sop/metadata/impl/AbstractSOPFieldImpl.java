/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.nio.ByteBuffer;

import com.woate.talkingbird.framework.communication.Formatter;
import com.woate.talkingbird.framework.communication.Parser;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
import com.woate.talkingbird.framework.core.converter.Converter;

/**
 * SOP字段对象的抽象实现<br>
 * 
 * @author liucheng 2014-12-16 下午02:35:27
 * 
 */
public abstract class AbstractSOPFieldImpl implements SOPVariableFieldWriteable, Formatter<Object, byte[]>, Parser<ByteBuffer, Object> {
	/**
	 * 该元信息的名称
	 */
	protected String name;
	/**
	 * 字段描述
	 */
	protected String desc;
	/**
	 * 该元信息的别名
	 */
	protected String alias;
	/**
	 * 该元信息所对应值对象的类全限定名
	 */
	protected Class<?> clazz;
	/**
	 * 该元信息在使用前调用的转换器的类全限定名
	 */
	protected Converter formatConverter;
	/**
	 * 该元信息在使用前调用的转换器的类全限定名
	 */
	protected Converter parseConverter;
	/**
	 * 编码集
	 */
	protected String encoding;
	/**
	 * 设置的值，一般用于初始化值、固定值或用户模式
	 */
	protected Object value;
	/**
	 * 值模式
	 */
	protected ValueMode valueMode;
	/**
	 * 数据格式
	 */
	protected String dataFormat;
	/**
	 * 允许Null
	 */
	protected boolean allowNull;
	
	//校验成员变量是否有效
	public void vildate(){
	}
	
	public AbstractSOPFieldImpl() {
		super();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getAlias() {
		return alias;
	}

	@Override
	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Converter getFormatConverter() {
		return formatConverter;
	}

	public void setFormatConverter(Converter formatConverter) {
		this.formatConverter = formatConverter;
	}

	public Converter getParseConverter() {
		return parseConverter;
	}

	public void setParseConverter(Converter parseConverter) {
		this.parseConverter = parseConverter;
	}

	@Override
	public String getEncoding() {
		return encoding;
	}

	@Override
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	@Override
	public Class<?> getClazz() {
		return clazz;
	}

	@Override
	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String getDesc() {
		return this.desc;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public ValueMode getValueMode() {
		return valueMode;
	}
	public void setValueMode(ValueMode valueMode) {
		this.valueMode = valueMode;
	}
	public String getDataFormat() {
		return dataFormat;
	}
	public void setDataFormat(String dataFormat) {
		this.dataFormat = dataFormat;
	}
	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	public boolean isAllowNull() {
		return allowNull;
	}

	public void setAllowNull(boolean allowNull) {
		this.allowNull = allowNull;
	}

}
