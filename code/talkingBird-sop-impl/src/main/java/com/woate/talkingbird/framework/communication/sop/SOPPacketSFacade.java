/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop;

import java.nio.ByteBuffer;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BootstrapMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.sop.interp.SOPEntity;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;

/**
 * SOP接口门面类<br>
 * 1.请求<br>
 * 系统信息头<br>
 * 交易公共头<br>
 * 交易数据头1<br>
 * 交易数据1<br>
 * ...<br>
 * 交易数据头n<br>
 * 交易数据n<br>
 * 
 * 2.应答<br>
 * 系统信息头<br>
 * 交易公共返回头<br>
 * 交易数据1<br>
 * ...<br>
 * 交易数据n<br>
 * 
 * @author liucheng 2014-11-24 上午10:27:42
 * 
 */
public class SOPPacketSFacade extends AbstractSOPPacket {

	public SOPPacketSFacade(String name, MainMetadataLoader<SOPMetadata> mainLoader) {
		super(name, mainLoader, new SOPMacGenCallback() {

			@Override
			public void call() {
			}
		}, new SOPMacVildateCallback() {

			@Override
			public void vildate() {
			}
		});
	}

	public SOPPacketSFacade(String name, BootstrapMetadataLoader<SOPMetadata> bootstrapLoader, SOPMacGenCallback callback, SOPMacVildateCallback Vildate) {
		super(name, bootstrapLoader.getLoader(name), callback, Vildate);
	}

	@Override
	public ByteBuffer format() throws TransformException {
		try {
			SOPSession session = SOPSession.getSession();
			session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
			int sopsize = 0;
			ByteBuffer sysbuffer = sys.format();
			sopsize += sysbuffer.position();
			sysbuffer.limit(sysbuffer.position());
			sysbuffer.rewind();
			ByteBuffer combuffer = com.format();
			sopsize += combuffer.position();
			combuffer.limit(combuffer.position());
			combuffer.rewind();
			ByteBuffer[] databuffers = new ByteBuffer[this.datas.size()];
			for (int i = 0; i < this.datas.size(); i++) {
				databuffers[i] = this.datas.get(i).format();
				sopsize += databuffers[i].position();
				databuffers[i].limit(databuffers[i].position());
				databuffers[i].rewind();
			}
			ByteBuffer buffer = ByteBuffer.allocate(sopsize);
			buffer.put(sysbuffer);
			buffer.put(combuffer);
			for (ByteBuffer byteBuffer : databuffers) {
				buffer.put(byteBuffer);
			}
			buffer.putShort(0, (short) buffer.limit());
			// 将包含SOP报文的缓冲区放入会话
			session.setAttribute(Dictionary.PACKET_SOP_MSG, buffer);
			// 生成MAC
			callback.call();
			return buffer;
		} catch (Exception e) {
			throw new TransformException("接口[" + mainLoader.getName() + "]在编排时发生异常！", e);
		}
	}

	@Override
	public void parse(ByteBuffer input) throws TransformException {
		try {
			SOPSession session = SOPSession.getSession();
			// 将包含SOP报文的缓冲区放入会话
			session.setAttribute(Dictionary.PACKET_SOP_MSG, input);
			// 验证MAC
			vildate.vildate();
			session.setAttribute(Dictionary.PACKET_TYPE, PacketType.RSP);
			sys.parse(input);
			com.parse(input);
			// 这里应该有交易是否是错误的情况的判断,然后使用不同的解析逻辑
			for (SOPEntity entity : this.datas) {
				entity.parse(input);
			}
		} catch (Exception e) {
			throw new TransformException("接口[" + mainLoader.getName() + "]在反编排时发生异常！", e);
		}
	}
}
