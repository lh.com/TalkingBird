/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop;

import java.util.ArrayList;
import java.util.List;

import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.sop.interp.SOPEntity;
import com.woate.talkingbird.framework.communication.sop.interp.busi.SOPBusiData;
import com.woate.talkingbird.framework.communication.sop.interp.header.SOPComHeader;
import com.woate.talkingbird.framework.communication.sop.interp.header.SOPSysHeader;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;

public abstract class AbstractSOPPacket implements SOPPacket {
	/**
	 * 交易别名<br>
	 * 如果有组合交易则使用能对该组合交易进行描述的名称<br>
	 * 如果不是组合交易则使用该交易码来进行描述<br>
	 */
	String name;
	/**
	 * SOP主加载器
	 */
	MainMetadataLoader<SOPMetadata> mainLoader;
	/**
	 * 系统信息头
	 */
	SOPEntity sys;
	/**
	 * 交易公共头
	 */
	SOPEntity com;

	/**
	 * 交易数据
	 */
	List<SOPEntity> datas = new ArrayList<SOPEntity>();
	/**
	 * 生成Mac的回调接口
	 */
	SOPMacGenCallback callback;
	/**
	 * 验证Mac的回调接口
	 */
	SOPMacVildateCallback vildate;
	public AbstractSOPPacket(String name, MainMetadataLoader<SOPMetadata> mainLoader, SOPMacGenCallback callback, SOPMacVildateCallback vildate) {
		super();
		this.name = name;
		this.mainLoader = mainLoader;
		this.callback = callback;
		this.vildate = vildate;
		this.sys = new SOPSysHeader(mainLoader.getSysHeaderLoader());
		this.com = new SOPComHeader(mainLoader.getComHeaderLoader());
		BusiDataMetadateLoader<SOPMetadata>[] busiDataLoaders = mainLoader.getBusiDataLoaders();
		for (BusiDataMetadateLoader<SOPMetadata> busiDataLoader : busiDataLoaders) {			
			this.datas.add(new SOPBusiData(busiDataLoader.getTradeCode(), busiDataLoader));
		}
	}
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}
}
