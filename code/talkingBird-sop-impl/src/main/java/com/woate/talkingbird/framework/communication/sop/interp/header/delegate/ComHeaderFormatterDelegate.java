/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.interp.header.delegate;

import java.nio.ByteBuffer;
import java.util.List;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.sop.SOPFormatter;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
/**
 * 公共信息头编排代理类
 * @author liucheng
 *
 */
public class ComHeaderFormatterDelegate implements SOPFormatter{
	/**
	 * 元信息加载器
	 */
	MetadataLoader<SOPMetadata> loader = null;
	public ComHeaderFormatterDelegate(MetadataLoader<SOPMetadata> loader) {
		this.loader = loader;
	}
	
	@Override
	public ByteBuffer format() throws TransformException {
		SOPSession session = SOPSession.getSession();
		PacketType packetType = session.getAttribute(Dictionary.PACKET_TYPE);
		List<SOPMetadata> fields = this.loader.listHeader(packetType);
		SOPMetadata fieldMetadata = null;
		ByteBuffer buffer = ByteBuffer.allocate(getHeaderSize(fields));
		for (int i = 0; i < fields.size(); i++) {
			fieldMetadata = fields.get(i);
			buffer.put(format0((SOPVariableField)fieldMetadata));
			//如果元信息存在错误代码,且不为7个A,则交易数据部分为异常包格式
			if(fieldMetadata.getName().equals(Dictionary.PTCWDH) && !"AAAAAAA".equals(session.getAttribute(Dictionary.PTCWDH))){				
				session.setAttribute(Dictionary.PACKET_TYPE, PacketType.EXP);
			}
		}
		return buffer;
	}
	
	byte[] format0(SOPVariableField fieldMetadata) throws TransformException {
		SOPSession session = SOPSession.getSession();
		Object value = session.getAttribute(fieldMetadata.getName());
		byte[] bytes = fieldMetadata.format(value);
		//如果需要编排的字段字节长度与描述不同,则根据描述中的信息进行截断
		if(bytes.length > fieldMetadata.getMaxLength()){
			//bytes = ArraysUtil.rtruncate(bytes, fieldMetadata.getMaxLength());
			throw new TransformException("字段["+fieldMetadata.getName()+"]元信息长度定义为["+ fieldMetadata.getMaxLength()+"]在编排时长度实际为["+bytes.length +"]!");
		}
		return bytes;
	}
	/**
	 * 获取系统信息头报文长度
	 * @return
	 */
	int getHeaderSize(List<SOPMetadata> fields){
		int size = 0;
		for (SOPMetadata field : fields) {
			size+= field.getMaxLength();
		}
		return size;
	}
}
