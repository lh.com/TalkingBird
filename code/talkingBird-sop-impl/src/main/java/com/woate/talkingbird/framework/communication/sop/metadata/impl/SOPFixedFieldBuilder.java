package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.math.BigDecimal;
import java.util.Date;

import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPFixedFieldWriteable;
import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.util.DateUtil;

public class SOPFixedFieldBuilder {
	String name;
	String alias;
	String desc;
	String encoding;
	int length;
	boolean rightFill;
	Class<?> clazz;
	Converter formatConverter;
	Converter parseConverter;
	Object value;
	ValueMode valueMode;
	
	public SOPFixedFieldBuilder(String name) {
		this.name = name;
	}
	public SOPFixedFieldWriteable build(){
		SOPFixedFieldImpl field = new SOPFixedFieldImpl();
		field.setName(name);
		field.setAlias(alias);
		field.setDesc(desc);
		field.setEncoding(encoding);
		field.setMinLength(length);
		field.setMaxLength(length);
		field.setRightFill(rightFill);
		field.setClazz(clazz);
		field.setFormatConverter(formatConverter);
		field.setParseConverter(parseConverter);
		field.setAllowNull(true);
		field.setValue(value);
		field.setValueMode(valueMode);
		//校验设置值是否通过
		field.vildate();
		return field;
	}
	public SOPFixedFieldWriteable clone(SOPFixedField oldField){
		SOPFixedFieldImpl field = new SOPFixedFieldImpl();
		field.setName(oldField.getName());
		field.setAlias(oldField.getAlias());
		field.setDesc(oldField.getDesc());
		field.setEncoding(oldField.getEncoding());
		field.setMinLength(oldField.getMinLength());
		field.setMaxLength(oldField.getMaxLength());
		field.setRightFill(oldField.isRightFill());
		field.setClazz(oldField.getClazz());
		field.setFormatConverter(oldField.getFormatConverter());
		field.setParseConverter(oldField.getParseConverter());
		field.setAllowNull(oldField.isAllowNull());
		field.setValue(oldField.getValue());
		field.setValueMode(oldField.getValueMode());
		//校验设置值是否通过
		field.vildate();
		return field;
	}
	public SOPFixedFieldBuilder setAlias(String alias) {
		this.alias = alias;
		return this;
	}
	public SOPFixedFieldBuilder setDesc(String desc) {
		this.desc = desc;
		return this;
	}
	public SOPFixedFieldBuilder setEncoding(String encoding) {
		this.encoding = encoding;
		return this;
	}
	public SOPFixedFieldBuilder setLength(int length) {
		this.length = length;
		return this;
	}
	public SOPFixedFieldBuilder setRightFill(boolean rightFill) {
		this.rightFill = rightFill;
		return this;
	}
	public SOPFixedFieldBuilder setClazz(Class<?> clazz) {
		this.clazz = clazz;
		return this;
	}
	public SOPFixedFieldBuilder setFormatConverter(Converter formatConverter) {
		this.formatConverter = formatConverter;
		return this;
	}
	public SOPFixedFieldBuilder setParseConverter(Converter parseConverter) {
		this.parseConverter = parseConverter;
		return this;
	}
	public SOPFixedFieldBuilder setValue(String value) {
		if(value.isEmpty()){
			return this;
		}
		//this.value = value;
		if (clazz == Integer.class) {
			this.value = new Integer(value);
		} else if (clazz == String.class) {
			this.value = value;
		} else if (clazz == BigDecimal.class) {
			this.value = new BigDecimal(value);
		} else if (clazz == Date.class) {
			this.value = DateUtil.toDate(value);
		}else{
			throw new IllegalArgumentException("字段["+ name+"]未设置值类型");
		}
		return this;
	}
	public SOPFixedFieldBuilder setValueMode(ValueMode valueMode) {
		this.valueMode = valueMode;
		return this;
	}
}
