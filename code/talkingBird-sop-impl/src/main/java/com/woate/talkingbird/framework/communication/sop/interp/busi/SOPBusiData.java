/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.interp.busi;

import java.nio.ByteBuffer;

import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.sop.SOPFormatter;
import com.woate.talkingbird.framework.communication.sop.SOPParser;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.interp.SOPEntity;
import com.woate.talkingbird.framework.communication.sop.interp.busi.delegate.BusiDataFormatterDelegate;
import com.woate.talkingbird.framework.communication.sop.interp.busi.delegate.BusiDataParserDelegate;
import com.woate.talkingbird.framework.communication.sop.interp.header.SOPBusiHeader;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;

/**
 * SOP业务数据<br>
 * 只需要根据输入自动进行SOP业务数据部分的转换<br>
 * 
 * @author liucheng 2014-12-11 下午02:04:13
 * 
 */
public class SOPBusiData implements SOPEntity{
	/**
	 * 交易码
	 */
	String tradeCode;
	/**
	 * 加载器
	 */
	BusiDataMetadateLoader<SOPMetadata> loader;
	/**
	 * 交易信息头
	 */
	SOPBusiHeader budiHeader;
	/**
	 * 编排器
	 */
	SOPFormatter formatter;
	/**
	 * 反编排器
	 */
	SOPParser parser;

	public SOPBusiData(String tradeCode, BusiDataMetadateLoader<SOPMetadata> loader) {
		this.tradeCode = tradeCode;
		this.loader = loader;
		this.budiHeader = new SOPBusiHeader(tradeCode, loader);
		this.formatter = new BusiDataFormatterDelegate(tradeCode, loader);
		this.parser = new BusiDataParserDelegate(tradeCode, loader);
	}

	public ByteBuffer format() throws TransformException {
		ByteBuffer headerbuffer = this.budiHeader.format();
		ByteBuffer databuffer = this.formatter.format();
		ByteBuffer buffer = ByteBuffer.allocate(headerbuffer.position() + databuffer.position());
		headerbuffer.limit(headerbuffer.position());
		headerbuffer.rewind();
		databuffer.limit(databuffer.position());
		databuffer.rewind();
		buffer.put(headerbuffer);
		buffer.put(databuffer);
		SOPSession session = SOPSession.getSession();
		session.pollFirstBusiData();
		return buffer;
	}

	public void parse(ByteBuffer input) throws TransformException {
		SOPSession session = SOPSession.getSession();
		session.createBusiData();
		this.budiHeader.parse(input);
		this.parser.parse(input);
	}

}
