/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.impl;

import java.nio.ByteBuffer;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.sop.SOPMacVildateCallback;
import com.woate.talkingbird.framework.communication.sop.SOPSession;

public class SOPMacVlidateCallbackImpl implements SOPMacVildateCallback {

	@Override
	public void vildate() {
		//将包含SOP报文的缓冲区放入会话
		ByteBuffer buffer = SOPSession.getSession().getAttribute(Dictionary.PACKET_SOP_MSG);
		int position = buffer.position();
		buffer.position(2);
		byte[] macBytes = new byte[16];
		buffer.get(macBytes);
		System.out.println(new String(macBytes));
		buffer.position(position);
	}

}
