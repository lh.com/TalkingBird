/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.interp.header.delegate;

import java.nio.ByteBuffer;
import java.util.List;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.sop.SOPParser;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
/**
 * 公共信息头反编排委托类<br>
 * @author liucheng
 * 2014-12-16 下午03:17:00
 *
 */
public class ComHeaderParserDelegate implements SOPParser{
	/**
	 * 元信息加载器
	 */
	MetadataLoader<SOPMetadata> loader;
	public ComHeaderParserDelegate(MetadataLoader<SOPMetadata> loader) {
		this.loader = loader;
	}

	@Override
	public void parse(ByteBuffer input) throws TransformException {
		SOPSession session = SOPSession.getSession();
		PacketType packetType = session.getAttribute(Dictionary.PACKET_TYPE);
		List<SOPMetadata> fields = this.loader.listHeader(packetType);
		SOPMetadata fieldMetadata = null;
		for (int i = 0; i < fields.size(); i++) {
			fieldMetadata = fields.get(i);
			Object value = parse0((SOPVariableField)fieldMetadata,input);
			session.setAttribute(fieldMetadata.getName(), value);
			//如果元信息存在错误代码,且不为7个A,则交易数据部分为异常包格式
			if(fieldMetadata.getName().equals(Dictionary.PTCWDH) && !"AAAAAAA".equals(session.getAttribute(Dictionary.PTCWDH))){				
				session.setAttribute(Dictionary.PACKET_TYPE, PacketType.EXP);
			}
		}
	}
	
	Object parse0(SOPVariableField fieldMetadata,ByteBuffer input) throws TransformException{
		return fieldMetadata.parse(input);
	}

}
