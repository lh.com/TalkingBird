/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPObjectWriteable;
/**
 * SOP对象的抽象实现<br>
 * @author liucheng
 * 2014-12-16 下午02:35:05
 *
 */
public abstract class AbstractSOPObjectImpl implements SOPObjectWriteable{
	/*
	 * 该元信息的名称
	 */
	protected String name;
	/**
	 * 该元信息的描述
	 */
	protected String desc;
	/**
	 * 该元信息的别名
	 */
	protected String alias;
	/**
	 * 编码集
	 */
	protected String encoding;
	/**
	 * 该对象包装为什么类
	 */
	protected Class<?> wrapClass;
	/**
	 * 该对象定义的描述信息
	 */
	protected List<SOPVariableField> fields = new ArrayList<SOPVariableField>();

	protected Map<String, Method> beanMethodCache = new HashMap<String, Method>();
	public AbstractSOPObjectImpl(String name, String alias, String desc, String encoding, Class<?> wrapClass) {
		this.name = name;
		this.desc = desc;
		this.alias = alias;
		this.encoding = encoding;
		this.wrapClass = wrapClass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Class<?> getWrapClass() {
		return wrapClass;
	}

	public void setWrapClass(Class<?> wrapClass) {
		this.wrapClass = wrapClass;
	}

	@Override
	public String getEncoding() {
		return encoding;
	}

	@Override
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	protected void init() {
		//如果已经初始化了
		if(!beanMethodCache.isEmpty()){
			return;
		}
		PropertyDescriptor[] properties = null;
		try {
			properties = Introspector.getBeanInfo(wrapClass).getPropertyDescriptors();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		for (PropertyDescriptor desc : properties) {
			beanMethodCache.put(desc.getName() + "@getter", desc.getReadMethod());
			beanMethodCache.put(desc.getName() + "@setter", desc.getWriteMethod());
		}
	}
	
	public List<SOPVariableField> getFields() {
		return fields;
	}

	public SOPVariableField getField(String name) {
		for (SOPVariableField field : fields) {
			if (field.getName().equals(name)) {
				return field;
			}
		}
		return null;
	}

	public void addField(SOPVariableField field) {
		fields.add(field);
	}

	public int getMaxLength() {
		int len = 0;
		for (SOPVariableField field : fields) {
			len += field.getMaxLength();
		}
		return len + getMinLength();
	}
	/**
	 * 格式化名字
	 * @return
	 */
	protected byte[] formatName(){
		return name.getBytes();
	}
	/**
	 * 解析名字
	 * @param bytes
	 * @return
	 */
	protected String parseName(byte[] bytes){
		return new String(bytes);
	}

	@Override
	public void setAlias(String alias) {
		this.alias = alias;
	}

	@Override
	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String getAlias() {
		return this.alias;
	}

	@Override
	public String getDesc() {
		return this.desc;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("[");
		builder.append("name:").append(name);
		builder.append(",alias:").append(alias);
		builder.append(",desc:").append(desc);
		builder.append(",encoding:").append(encoding);
		builder.append(",wrapClass:").append(wrapClass);
		builder.append(",fields:");
		for (SOPVariableField field : fields) {
			builder.append(field.toString());
			builder.append(",");
		}
		builder.append("]\n");
		return builder.toString();
	}
	
}
