/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata;

import com.woate.talkingbird.framework.communication.metadata.Metadata;
/**
 * SOP元信息基类接口<br>
 * 该接口是对所有SOP报文中涉及的元信息的基接口，提供对SOP报文不可分单元的描述。<br>
 * @author liucheng
 * 2014-12-16 下午02:05:30
 *
 */
public interface SOPMetadata extends Metadata{
	/**
	 * 最大长度<br>
	 * SOP在不延续情况下支持的最大一个不可分单元最大长度
	 */
	public static final int MAX_LENGTH = 0xFA;
	/**
	 * 预留控制符1<br>
	 */
	public static final int CONTROL1 = 0xFB;
	/**
	 * 预留控制符2<br>
	 */
	public static final int CONTROL2 = 0xFC;
	/**
	 * 预留控制符3<br>
	 */
	public static final int CONTROL3 = 0xFD;
	/**
	 * 预留控制符4<br>
	 */
	public static final int CONTROL4 = 0xFE;
	/**
	 * 延续标志<br>
	 * 一个不可分单元长度超过最大长度时使用该标记进行延续<br>
	 */
	public static final int CONTINUANCE = 0xFF;
	/**
	 * 元信息别名
	 * @return
	 */
	public String getAlias();
	/**
	 * 元信息描述
	 * @return
	 */
	public String getDesc();
	/**
	 * 获取编码集
	 * @return
	 */
	public String getEncoding();
	/**
	 * 获取元信息描述的字段最大长度
	 * @return
	 */
	public int getMaxLength();
	/**
	 * 获取元信息描述的字段最小长度
	 * @return
	 */
	public int getMinLength();
}
