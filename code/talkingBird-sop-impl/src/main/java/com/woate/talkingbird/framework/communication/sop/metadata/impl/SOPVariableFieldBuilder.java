package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.math.BigDecimal;
import java.util.Date;

import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.util.DateUtil;

/**
 * SOP变长字段建造器
 * @author liucheng
 *
 */
public class SOPVariableFieldBuilder {
	String name;
	String alias;
	String desc;
	String encoding;
	int minLength;
	int maxLength;
	Class<?> clazz;
	Converter formatConverter;
	Converter parseConverter;
	boolean allowNull;
	Object value;
	ValueMode valueMode;
	
	public SOPVariableFieldBuilder(String name) {
		this.name = name;
	}
	public SOPVariableFieldWriteable build(){
		SOPVariableFieldImpl field = new SOPVariableFieldImpl();
		field.setName(name);
		field.setAlias(alias);
		field.setDesc(desc);
		field.setEncoding(encoding);
		field.setMinLength(minLength);
		field.setMaxLength(maxLength);
		field.setClazz(clazz);
		field.setFormatConverter(formatConverter);
		field.setParseConverter(parseConverter);
		field.setAllowNull(allowNull);
		field.setValue(value);
		field.setValueMode(valueMode);
		//校验设置值是否通过
		field.vildate();
		return field;
	}
	public SOPVariableFieldWriteable clone(SOPVariableField oldField){
		SOPVariableFieldImpl field = new SOPVariableFieldImpl();
		field.setName(oldField.getName());
		field.setAlias(oldField.getAlias());
		field.setDesc(oldField.getDesc());
		field.setEncoding(oldField.getEncoding());
		field.setMinLength(oldField.getMinLength());
		field.setMaxLength(oldField.getMaxLength());
		field.setClazz(oldField.getClazz());
		field.setFormatConverter(oldField.getFormatConverter());
		field.setParseConverter(oldField.getParseConverter());
		field.setAllowNull(oldField.isAllowNull());
		field.setValue(oldField.getValue());
		field.setValueMode(oldField.getValueMode());
		//校验设置值是否通过
		field.vildate();
		return field;
	}
	public SOPVariableFieldBuilder setAlias(String alias) {
		this.alias = alias;
		return this;
	}
	public SOPVariableFieldBuilder setDesc(String desc) {
		this.desc = desc;
		return this;
	}
	public SOPVariableFieldBuilder setEncoding(String encoding) {
		this.encoding = encoding;
		return this;
	}
	public SOPVariableFieldBuilder setMinLength(int minLength) {
		this.minLength = minLength;
		return this;
	}
	public SOPVariableFieldBuilder setMaxLength(int maxLength) {
		this.maxLength = maxLength;
		return this;
	}
	public SOPVariableFieldBuilder setClazz(Class<?> clazz) {
		this.clazz = clazz;
		return this;
	}
	public SOPVariableFieldBuilder setFormatConverter(Converter formatConverter) {
		this.formatConverter = formatConverter;
		return this;
	}
	public SOPVariableFieldBuilder setParseConverter(Converter parseConverter) {
		this.parseConverter = parseConverter;
		return this;
	}
	public SOPVariableFieldBuilder setAllowNull(boolean allowNull) {
		this.allowNull = allowNull;
		return this;
	}
	public SOPVariableFieldBuilder setValue(String value) {
		if(value.isEmpty()){
			return this;
		}
		//this.value = value;
		if (clazz == Integer.class) {
			this.value = Integer.parseInt(value);
		} else if (clazz == String.class) {
			this.value = value;
		} else if (clazz == BigDecimal.class) {
			this.value = new BigDecimal(value);
		} else if (clazz == Byte.class) {
			this.value = Byte.valueOf(value);
		} else if (clazz == Date.class) {
			this.value = DateUtil.toDate(value);
		}else{
			throw new IllegalArgumentException("字段["+ name+"]未设置值类型");
		}
		return this;
	}
	public SOPVariableFieldBuilder setValueMode(ValueMode valueMode) {
		this.valueMode = valueMode;
		return this;
	}

}
