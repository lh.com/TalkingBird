/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata;


import java.nio.ByteBuffer;

import com.woate.talkingbird.framework.communication.Formatter;
import com.woate.talkingbird.framework.communication.Parser;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.core.converter.Converter;
/**
 * SOP不定长字段对象<br>
 * 一个不定长报文内容前都使用一个字节的长度描述信息<br>
 * 最大描述0xFA长度，超过长度使用0xFF描述续行<br>
 * @author liucheng
 * 2014-12-16 下午02:22:05
 *
 */
public interface SOPVariableField extends SOPMetadata,Formatter<Object, byte[]>, Parser<ByteBuffer, Object>{
	/**
	 * 获取编排时使用的转换器<br>
	 * 用于完成字节数组与目标对象数据之间的转换<br>
	 * @return
	 */
	public Converter getFormatConverter();
	/**
	 * 获取反编排时使用的转换器<br>
	 * 用于完成字节数组与目标对象数据之间的转换<br>
	 * @return
	 */
	public Converter getParseConverter();
	/**
	 * 获取最大的SOP报文长度<br>
	 * 区别于getMaxLength();
	 * @return
	 */
	public int getMaxSOPLength();
	/**
	 * 获取最小的SOP报文长度<br>
	 * 区别于getMinLength();
	 * @return
	 */
	public int getMinSOPLength();
	/**
	 * 获取字段对应的类型
	 * @return
	 */
	public Class<?> getClazz();
	/**
	 * 是否在编排时允许为null
	 * @return
	 */
	public boolean isAllowNull();
	/**
	 * 获取默认值或者固定值
	 * @return
	 */
	public Object getValue();
	/**
	 * 获取值模式
	 * @return
	 */
	public ValueMode getValueMode();
	/**
	 * 获取数据格式掩码
	 * @return
	 */
	public String getDataFormat();

}
