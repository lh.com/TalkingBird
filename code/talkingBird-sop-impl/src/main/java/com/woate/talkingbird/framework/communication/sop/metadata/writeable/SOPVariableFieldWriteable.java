/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata.writeable;


import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
import com.woate.talkingbird.framework.core.converter.Converter;
/**
 * SOP不定长字段对象宽接口，提供改变元信息描述的能力<br>
 * @author liucheng
 * 2014-12-16 下午02:26:41
 *
 */
public interface SOPVariableFieldWriteable extends SOPVariableField,SOPMetadataWriteable{
	/**
	 * 设置编排时使用的转换器<br>
	 * 用于完成字节数组与目标对象数据之间的转换<br>
	 * @param converter
	 */
	public void setFormatConverter(Converter formatConverter);
	/**
	 * 设置反编排时使用的转换器<br>
	 * 用于完成字节数组与目标对象数据之间的转换<br>
	 * @param converter
	 */
	public void setParseConverter(Converter parseConverter);
	/**
	 * 设置元信息描述的字段最大长度
	 * @param maxLength
	 */
	public void setMaxLength(int maxLength);
	/**
	 * 设置元信息描述的字段最小长度
	 * @param minLength
	 */
	public void setMinLength(int minLength);
	/**
	 * 设置在编排时是否允许为null
	 * @param allowNull
	 */
	public void setAllowNull(boolean allowNull);
	/**
	 * 设置默认值或者固定值
	 * @param value
	 */
	public void setValue(Object value);
	/**
	 * 设置取值模式
	 * @param valueMode
	 */
	public void setValueMode(ValueMode valueMode);
	/**
	 * 设置数据格式
	 * @param dataFormat
	 */
	public void setDataFormat(String dataFormat);
}
