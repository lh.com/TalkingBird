/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata.writeable;

import com.woate.talkingbird.framework.communication.sop.metadata.SOPObject;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;

/**
 * SOP对象宽接口，提供了改变元信息描述信息的能力<br>
 * 
 * @author liucheng 2014-11-24 下午05:13:25
 * 
 */
public interface SOPObjectWriteable extends SOPObject, SOPMetadataWriteable {
	/**
	 * 设置SOP包装类型
	 * @param clazz
	 */
	public void setWrapClass(Class<?> clazz);
	/**
	 * 增加字段对象描述元信息
	 * @param field 字段
	 */
	public void addField(SOPVariableField field);
}
