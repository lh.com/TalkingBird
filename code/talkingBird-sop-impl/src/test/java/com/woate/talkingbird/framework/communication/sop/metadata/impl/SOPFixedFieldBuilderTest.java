package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;

public class SOPFixedFieldBuilderTest {
	@Test
	public void testBuild(){
		String name = "name1";
		String alias = "alias1";
		String desc = "desc1";
		String encoding = "encoding1";
		int length = 1;
		Class<?> clazz = String.class;
		Converter formatConverter = new String2ByteArrayConverter();
		Converter parseConverter = new ByteArray2StringConverter();
		String value = "value1";
		ValueMode valueMode = ValueMode.DEFAULT;
		SOPFixedFieldBuilder builder = new SOPFixedFieldBuilder(name);
		SOPFixedField field = builder.setAlias(alias).setDesc(desc).setEncoding(encoding).setLength(length).setClazz(clazz).setFormatConverter(formatConverter).setParseConverter(parseConverter).setValue(value).setValueMode(valueMode).build();
		Assert.assertEquals(name, field.getName());
		Assert.assertEquals(alias, field.getAlias());
		Assert.assertEquals(desc, field.getDesc());
		Assert.assertEquals(encoding, field.getEncoding());
		Assert.assertEquals(length, field.getMinLength());
		Assert.assertEquals(length, field.getMaxLength());
		Assert.assertEquals(clazz, field.getClazz());
		Assert.assertEquals(formatConverter, field.getFormatConverter());
		Assert.assertEquals(parseConverter, field.getParseConverter());
		Assert.assertEquals(value, field.getValue());
		Assert.assertEquals(valueMode, field.getValueMode());
		System.out.println(field);
	}
}
