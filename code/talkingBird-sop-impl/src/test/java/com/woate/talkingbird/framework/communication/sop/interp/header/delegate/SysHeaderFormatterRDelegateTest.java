package com.woate.talkingbird.framework.communication.sop.interp.header.delegate;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.interp.header.delegate.SysHeaderFormatterDelegate;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldBuilder;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldImpl;
import com.woate.talkingbird.framework.core.AliasNameManager;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2IntegerConverter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.Integer2ByteArrayConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
import com.woate.talkingbird.framework.util.Hex;

public class SysHeaderFormatterRDelegateTest {
	@Test
	public void test1() throws TransformException {
		SysHeaderFormatterDelegate formatter = new SysHeaderFormatterDelegate(new MetadataLoader() {
			@Override
			public MetadataLoader load() {
				return this;
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				List<SOPMetadata> fields = new ArrayList<SOPMetadata>();
				SOPFixedField field1 = new SOPFixedFieldBuilder("SHJBCD").setAlias("数据包长度").setLength(2).setClazz(Integer.class).setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
				SOPFixedField field2 = new SOPFixedFieldBuilder("BAWMAC").setAlias("报文MAC").setLength(16).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				fields.add(field1);
				fields.add(field2);
				return fields;
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}

		});
		SOPSession session = SOPSession.getSession();
		session.setAttribute("SHJBCD", 0x1D);
		session.setAttribute("BAWMAC", "1234567890abcdef");
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		ByteBuffer buffer = formatter.format();
		System.out.println(Hex.toDisplayString(buffer.array()));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("001D31323334353637383930616263646566"), buffer.array()));
		SOPSession.destory();
	}

	@Test
	public void test2() throws TransformException {
		SysHeaderFormatterDelegate formatter = new SysHeaderFormatterDelegate(new MetadataLoader() {

			@Override
			public MetadataLoader load() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				List<SOPMetadata> fields = new ArrayList<SOPMetadata>();
				SOPFixedField field1 = new SOPFixedFieldBuilder("SHJBCD").setAlias("数据包长度").setLength(2).setClazz(Integer.class).setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
				SOPFixedField field2 = new SOPFixedFieldBuilder("BAWMAC").setAlias("报文MAC").setLength(16).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				fields.add(field1);
				fields.add(field2);
				return fields;
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}

		});
		SOPSession session = SOPSession.getSession();
		session.setAttribute("SHJBCD", 0x1D);
		session.setAttribute("BAWMAC", "1234567890abcdef");
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		ByteBuffer buffer = formatter.format();
		System.out.println(Hex.toDisplayString(buffer.array()));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("001D31323334353637383930616263646566"), buffer.array()));
		SOPSession.destory();
	}

	@Test
	public void test3() throws TransformException {
		SysHeaderFormatterDelegate formatter = new SysHeaderFormatterDelegate(new MetadataLoader() {

			@Override
			public MetadataLoader load() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				List<SOPMetadata> fields = new ArrayList<SOPMetadata>();
				SOPFixedField field1 = new SOPFixedFieldBuilder("SHJBCD").setAlias("数据包长度").setLength(2).setClazz(Integer.class).setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
				SOPFixedField field2 = new SOPFixedFieldBuilder("BAWMAC").setAlias("报文MAC").setLength(16).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				fields.add(field1);
				fields.add(field2);
				return fields;
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}

		});
		SOPSession session = SOPSession.getSession();
		session.setAttribute("SHJBCD", 0x1D);
		session.setAttribute("BAWMAC", "1234567890abcdef");
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		ByteBuffer buffer = formatter.format();
		System.out.println(Hex.toDisplayString(buffer.array()));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("001D31323334353637383930616263646566"), buffer.array()));
		SOPSession.destory();
	}
}
