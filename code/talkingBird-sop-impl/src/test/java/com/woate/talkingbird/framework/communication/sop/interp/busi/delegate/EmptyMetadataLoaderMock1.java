package com.woate.talkingbird.framework.communication.sop.interp.busi.delegate;

import java.util.ArrayList;
import java.util.List;

import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.core.AliasNameManager;
/**
 * 空的元信息加载器
 * @author liucheng
 * 2014-12-22 下午01:11:55
 *
 */
public class EmptyMetadataLoaderMock1 implements MetadataLoader {

	@Override
	public MetadataLoader load() {
		return this;
	}

	@Override
	public List<SOPMetadata> listHeader(PacketType type) {
		return new ArrayList<SOPMetadata>();
	}

	@Override
	public AliasNameManager getAliasNameManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MainMetadataLoader getMainLoader() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getHeadFileName() {
		// TODO 自动生成的方法存根
		return null;
	}

}
