package com.woate.talkingbird.framework.communication.sop.interp.busi.delegate;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.interp.busi.delegate.BusiDataFormatterDelegate;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.Bean1;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPBeanImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldBuilder;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPBeanWriteable;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
import com.woate.talkingbird.framework.core.AliasNameManager;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
import com.woate.talkingbird.framework.util.Hex;

public class BusiDataFormatterDelegateTest {
	@Test
	public void testFormat(){
		SOPSession.destory();
		final List<SOPMetadata> list = new ArrayList<SOPMetadata>();
		SOPBeanWriteable obj0 = new SOPBeanImpl("O900b0","", "ChangeKey1","UTF-8", Bean1.class);
		SOPVariableFieldWriteable field0 = new SOPVariableFieldBuilder("name").setAlias("密钥名0").setMinLength(1).setMaxLength(10).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		obj0.addField(field0);
		list.add(obj0);
		BusiDataMetadateLoader busiDataLoader = new BusiDataMetadateLoader(){
			@Override
			public BusiDataMetadateLoader load() {
				return this;
			}
			
			@Override
			public List<SOPMetadata> list(PacketType type) {
				return list;
			}

			@Override
			public String getTradeCode() {
				return "900b";
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				return new ArrayList<SOPMetadata>();
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}


			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getHeaderAliasNameManager() {
				// TODO 自动生成的方法存根
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}
			
		};
		BusiDataFormatterDelegate formatter = new BusiDataFormatterDelegate("900b", busiDataLoader);
		Bean1 bean1= new Bean1();
		bean1.setName("测试");
		SOPBusiWrapper wrapper = SOPSession.getSession().createBusiData();
		wrapper.setBusidata("O900b0", bean1);
		SOPSession session = SOPSession.getSession();
		ByteBuffer buffer = null;
		try {
			buffer = formatter.format();
		} catch (TransformException e) {
			e.printStackTrace();
		}
		byte[] bytes = new byte[buffer.position()];
		buffer.flip();
		buffer.get(bytes);
		Hex.write(bytes);
	}
}
