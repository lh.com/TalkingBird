package com.woate.talkingbird.framework.communication.sop.interp.busi.delegate;

import com.woate.talkingbird.framework.communication.loader.BootstrapMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.core.AliasNameManager;

public class MainMetadataLoaderMock1 implements MainMetadataLoader{
	public BusiDataMetadateLoader[] busiDataLoader;
	public MetadataLoader comHeaderLoader;
	public MetadataLoader sysHeaderLoader;
	
	@Override
	public BusiDataMetadateLoader[] getBusiDataLoaders() {
		return busiDataLoader;
	}

	@Override
	public MetadataLoader getComHeaderLoader() {
		return comHeaderLoader;
	}

	@Override
	public MetadataLoader getSysHeaderLoader() {
		return sysHeaderLoader;
	}

	@Override
	public MainMetadataLoader load() {
		return this;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AliasNameManager getAliasNameManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BootstrapMetadataLoader getBoostrapLoader() {
		// TODO Auto-generated method stub
		return null;
	}

}
