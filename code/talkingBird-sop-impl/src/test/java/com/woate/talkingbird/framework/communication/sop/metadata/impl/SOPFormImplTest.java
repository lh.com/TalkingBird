package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFormImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPFormWriteable;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2IntegerConverter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.Integer2ByteArrayConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
import com.woate.talkingbird.framework.util.Hex;

public class SOPFormImplTest {
	@Test
	public void testFormat1() throws Exception {
		Bean1 bean1 = new Bean1("tom", 25);
		Bean1 bean2 = new Bean1("jack", 11);
		List<Bean1> list = new ArrayList<Bean1>();
		list.add(bean1);
		list.add(bean2);
		SOPFormWriteable<Bean1> form = new SOPFormImpl<Bean1>("Fform1", "", "form1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		form.addField(field1);
		form.addField(field2);
		byte[] bytes = form.format(list);
		System.out.println(Hex.toDisplayString(bytes));
		String hex = "0646666F726D31020203746F6D0119046A61636B010B";
		Assert.assertTrue(Arrays.equals(Hex.fromHexString(hex), bytes));
	}

	@Test(expected = TransformException.class)
	public void testFormat2() throws Exception {
		Bean1 bean1 = new Bean1("tom", 25);
		Bean1 bean2 = new Bean1("jack", 11);
		List<Bean1> list = new ArrayList<Bean1>();
		list.add(bean1);
		list.add(bean2);
		SOPFormWriteable<Bean1> form = new SOPFormImpl<Bean1>("Fform1", "", "form1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field3 = new SOPVariableFieldBuilder("sex").setDesc("sex").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		form.addField(field1);
		form.addField(field2);
		form.addField(field3);
		form.format(list);
		Assert.fail();
	}

	@Test
	public void testFormat3() throws Exception {
		Bean1 bean1 = new Bean1("tom", 25);
		Bean1 bean2 = new Bean1("jack", 11);
		List<Bean1> list = new ArrayList<Bean1>();
		list.add(bean1);
		list.add(bean2);
		SOPFormWriteable<Bean1> form = new SOPFormImpl<Bean1>("Fform1", "", "form1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		form.addField(field1);
		byte[] bytes = form.format(list);
		System.out.println(Hex.toDisplayString(bytes));
		String hex = "0646666f726d31020103746f6d046a61636b";
		Assert.assertTrue(Arrays.equals(Hex.fromHexString(hex), bytes));
	}

	@Test
	public void testFormat4() throws Exception {
		String m = "";
		for (int i = 0; i < 53; i++) {
			m += "12345";
		}
		Bean1 bean1 = new Bean1(m, 25);
		List<Bean1> list = new ArrayList<Bean1>();
		list.add(bean1);
		SOPFormWriteable<Bean1> form = new SOPFormImpl<Bean1>("Fform1", "", "form1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(1).setMaxLength(53*5).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		form.addField(field1);
		byte[] bytes = form.format(list);
		System.out.println(Hex.toDisplayString(bytes));
		String hex = "0646666F726D310101FF313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334350F313233343531323334353132333435";
		Assert.assertTrue(Arrays.equals(Hex.fromHexString(hex), bytes));
	}

	@Test
	public void testFormat5() throws Exception {
		String m = "";
		for (int i = 0; i < 53; i++) {
			m += "12345";
		}
		Bean1 bean1 = new Bean1(m, 25);
		Bean1 bean2 = new Bean1(m + m, 25);
		List<Bean1> list = new ArrayList<Bean1>();
		list.add(bean1);
		list.add(bean2);
		SOPFormWriteable<Bean1> form = new SOPFormImpl<Bean1>("Fform1","", "form1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(53*5).setMaxLength(53*5*2).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		form.addField(field1);
		byte[] bytes = form.format(list);
		System.out.println(Hex.toHexString(bytes));
		String hex = "0646666F726D310201FF313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334350F313233343531323334353132333435FF31323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435FF313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334351E313233343531323334353132333435313233343531323334353132333435";
		Assert.assertTrue(Arrays.equals(Hex.fromHexString(hex), bytes));
	}
	@Test
	public void testFormat6() throws Exception {
		String m = "";
		for (int i = 0; i < 53; i++) {
			m += "12345";
		}
		Bean1 bean1 = new Bean1(m, 25);
		Bean1 bean2 = new Bean1(m + m, 12);
		List<Bean1> list = new ArrayList<Bean1>();
		list.add(bean1);
		list.add(bean2);
		SOPFormWriteable<Bean1> form = new SOPFormImpl<Bean1>("Fform1", "", "form1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(53*5).setMaxLength(53*5*2).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		form.addField(field1);
		form.addField(field2);
		byte[] bytes = form.format(list);
		System.out.println(Hex.toHexString(bytes));
	}
	@Test
	public void testParse1() throws Exception {
		String m = "12345";
		Bean1 bean1 = new Bean1(m, 25);
		List<Bean1> list = new ArrayList<Bean1>();
		list.add(bean1);
		SOPFormWriteable<Bean1> form = new SOPFormImpl<Bean1>("Fform1", "", "form1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(5).setMaxLength(10).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		form.addField(field1);
		form.addField(field2);
		byte[] bytes = form.format(list);
		System.out.println(Hex.toHexString(bytes));
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		buffer.rewind();
		List<Bean1> list0 = form.parse(buffer);
		Assert.assertEquals(m, list0.get(0).name);
		Assert.assertEquals(25, list0.get(0).age);
	}
	@Test
	public void testParse2() throws Exception {
		String m = "��ķ";
		Bean1 bean1 = new Bean1(m, 25);
		List<Bean1> list = new ArrayList<Bean1>();
		list.add(bean1);
		SOPFormWriteable<Bean1> form = new SOPFormImpl<Bean1>("Fform1","", "form1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(4).setMaxLength(10).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		form.addField(field1);
		form.addField(field2);
		byte[] bytes = form.format(list);
		System.out.println(Hex.toHexString(bytes));
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		buffer.rewind();
		List<Bean1> list0 = form.parse(buffer);
		Assert.assertEquals(m, list0.get(0).name);
		Assert.assertEquals(25, list0.get(0).age);
	}
	@Test
	public void testParse3() throws Exception {
		String m = "��ķ";
		Bean1 bean1 = new Bean1(m, 25);
		Bean1 bean2 = new Bean1(m + m, 50);
		List<Bean1> list = new ArrayList<Bean1>();
		list.add(bean1);
		list.add(bean2);
		SOPFormWriteable<Bean1> form = new SOPFormImpl<Bean1>("Fform1", "", "form1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(1).setMaxLength(12).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(12).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		form.addField(field1);
		form.addField(field2);
		byte[] bytes = form.format(list);
		System.out.println(Hex.toHexString(bytes));
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		buffer.rewind();
		List<Bean1> list0 = form.parse(buffer);
		Assert.assertEquals(2, list0.size());
		Assert.assertEquals(m, list0.get(0).name);
		Assert.assertEquals(25, list0.get(0).age);
		Assert.assertEquals(m + m, list0.get(1).name);
		Assert.assertEquals(50, list0.get(1).age);
	}
	@Test
	public void testParse4() throws Exception {
		String m = "";
		for (int i = 0; i < 53; i++) {
			m += "12345";
		}
		Bean1 bean1 = new Bean1(m, 25);
		Bean1 bean2 = new Bean1(m + m, 50);
		List<Bean1> list = new ArrayList<Bean1>();
		list.add(bean1);
		list.add(bean2);
		SOPFormWriteable<Bean1> form = new SOPFormImpl<Bean1>("Fform1","",  "form1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(53*5).setMaxLength(53*5*2).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		form.addField(field1);
		form.addField(field2);
		byte[] bytes = form.format(list);
		System.out.println(Hex.toHexString(bytes));
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		buffer.rewind();
		List<Bean1> list0 = form.parse(buffer);
		Assert.assertEquals(2, list0.size());
		Assert.assertEquals(m, list0.get(0).name);
		System.out.println(list0.get(0).name);
		Assert.assertEquals(25, list0.get(0).age);
		System.out.println(list0.get(0).age);
		Assert.assertEquals(m + m, list0.get(1).name);
		System.out.println(list0.get(1).name);
		Assert.assertEquals(50, list0.get(1).age);
		System.out.println(list0.get(1).age);
	}
}
