package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;

public class SOPVariableFieldBuilderTest {
	@Test
	public void testBuild(){
		String name = "name1";
		String alias = "alias1";
		String desc = "desc1";
		String encoding = "encoding1";
		int minLength = 1;
		int maxLength = 2;
		Class<?> clazz = String.class;
		Converter formatConverter = new String2ByteArrayConverter();
		Converter parseConverter = new ByteArray2StringConverter();
		boolean allowNull = true;
		String value = "value1";
		ValueMode valueMode = ValueMode.DEFAULT;
		SOPVariableFieldBuilder builder = new SOPVariableFieldBuilder(name);
		SOPVariableField field = builder.setAlias(alias).setDesc(desc).setEncoding(encoding).setMinLength(minLength).setMaxLength(maxLength).setClazz(clazz).setFormatConverter(formatConverter).setParseConverter(parseConverter).setAllowNull(allowNull).setValue(value).setValueMode(valueMode).build();
		Assert.assertEquals(name, field.getName());
		Assert.assertEquals(alias, field.getAlias());
		Assert.assertEquals(desc, field.getDesc());
		Assert.assertEquals(encoding, field.getEncoding());
		Assert.assertEquals(minLength, field.getMinLength());
		Assert.assertEquals(maxLength, field.getMaxLength());
		Assert.assertEquals(clazz, field.getClazz());
		Assert.assertEquals(formatConverter, field.getFormatConverter());
		Assert.assertEquals(parseConverter, field.getParseConverter());
		Assert.assertEquals(allowNull, field.isAllowNull());
		Assert.assertEquals(value, field.getValue());
		Assert.assertEquals(valueMode, field.getValueMode());
		System.out.println(field);
	}
}
