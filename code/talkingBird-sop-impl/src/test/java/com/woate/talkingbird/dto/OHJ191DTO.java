package com.woate.talkingbird.dto;

public class OHJ191DTO {
	Integer totalAmount;

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public String toString() {
		return "OHJ191DTO [totalAmount=" + totalAmount + "]";
	}
	
}
