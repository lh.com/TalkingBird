package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2DateConverter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.Date2ByteArrayConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
import com.woate.talkingbird.framework.util.DateUtil;
import com.woate.talkingbird.framework.util.Hex;

public class SOPVariableFieldImplTest {
	@Test
	public void testFormat1() throws Exception {
		SOPVariableFieldWriteable field = new SOPVariableFieldBuilder("TRADER").setDesc("交易员").setMinLength(5).setMaxLength(5).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setAllowNull(false)
				.setValueMode(ValueMode.USER).build();
		String m = "12345";
		// 20
		byte[] bytes = field.format(m);
		System.out.println(Hex.toDisplayString(bytes));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("053132333435"), bytes));
	}

	@Test
	public void testFormat2() throws Exception {
		SOPVariableFieldWriteable field = new SOPVariableFieldBuilder("TRADER").setDesc("交易员").setMinLength(265).setMaxLength(265).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setAllowNull(false)
				.setValueMode(ValueMode.USER).build();
		String m = "";
		String hex = "FF313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334353132333435313233343531323334350F313233343531323334353132333435";
		for (int i = 0; i < 53; i++) {
			m += "12345";
		}
		byte[] bytes = field.format(m);
		System.out.println(Hex.toHexString(bytes));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString(hex), bytes));
	}

	@Test
	public void testFormat3() throws Exception {
		SOPVariableFieldWriteable field = new SOPVariableFieldBuilder("TRADER").setDesc("交易员").setMinLength(14).setMaxLength(21).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setAllowNull(false)
				.setValueMode(ValueMode.USER).setEncoding("GBK").build();
		String m = "一一一一一一一";
		byte[] bytes = field.format(m);
		System.out.println(Hex.toHexString(bytes));
		Assert.assertEquals("0ED2BBD2BBD2BBD2BBD2BBD2BBD2BB", Hex.toHexString(bytes));
	}
	@Test(expected = TransformException.class)
	public void testFormat4() throws Exception {
		SOPVariableFieldWriteable field = new SOPVariableFieldBuilder("TRADER").setDesc("交易员").setMinLength(8).setMaxLength(10).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setAllowNull(false)
				.setValueMode(ValueMode.USER).build();
		String m = "这其实是个测试";
		byte[] bytes = field.format(m);
	}
	@Test(expected = TransformException.class)
	public void testFormat5() throws Exception {
		SOPVariableFieldWriteable field = new SOPVariableFieldBuilder("TRADER").setDesc("交易员").setMinLength(15).setMaxLength(18).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setAllowNull(false)
				.setValueMode(ValueMode.USER).build();
		String m = "这其实是个测试";
		byte[] bytes = field.format(m);
	}

	@Test
	public void testParse1() throws Exception {
		SOPVariableFieldWriteable field = new SOPVariableFieldBuilder("TRADER").setDesc("交易员").setMinLength(5).setMaxLength(5).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setAllowNull(false)
				.setValueMode(ValueMode.USER).build();
		byte[] bytes = new byte[]{(byte) 0x05, (byte) 0x31, (byte) 0x32, (byte) 0x33, (byte) 0x34, (byte) 0x35};
		ByteBuffer input = ByteBuffer.wrap(bytes);
		Object val = field.parse(input);
		Assert.assertEquals("12345", val);
	}

	@Test
	public void testParse2() throws Exception {
		String msg = "一一一一";
		SOPVariableFieldWriteable field = new SOPVariableFieldBuilder("TRADER").setDesc("交易员").setMinLength(12).setMaxLength(12).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setAllowNull(false)
				.setValueMode(ValueMode.USER).build();
		byte[] bytes = field.format(msg);
		System.out.println(Hex.toHexString(bytes));
		Assert.assertEquals("0CE4B880E4B880E4B880E4B880", Hex.toHexString(bytes));
		Object val = field.parse(ByteBuffer.wrap(bytes));
		Assert.assertEquals(msg, val);
	}

	@Test
	public void testParse3() throws Exception {
		String m = "";
		for (int i = 0; i < 53; i++) {
			m += "12345";
		}
		SOPVariableFieldWriteable field = new SOPVariableFieldBuilder("TRADER").setDesc("交易员").setMinLength(53*5).setMaxLength(53*5).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setAllowNull(false)
				.setValueMode(ValueMode.USER).build();
		byte[] bytes = field.format(m);
		System.out.println(Hex.toHexString(bytes));
		Object val = field.parse(ByteBuffer.wrap(bytes));
		Assert.assertEquals(m, val);
	}
	@Test(expected = TransformException.class)
	public void testParse4() throws Exception {
		String m = "";
		for (int i = 0; i < 53; i++) {
			m += "12345";
		}
		SOPVariableFieldWriteable field = new SOPVariableFieldBuilder("TRADER").setDesc("交易员").setMinLength(53*5 + 2).setMaxLength(53 * 5 + 3).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setAllowNull(false)
				.setValueMode(ValueMode.USER).build();
		byte[] bytes = field.format(m);
		System.out.println(Hex.toHexString(bytes));
		Object val = field.parse(ByteBuffer.wrap(bytes));
		Assert.assertEquals(m, val);
	}
	@Test(expected = TransformException.class)
	public void testParse5() throws Exception {
		String m = "";
		for (int i = 0; i < 53; i++) {
			m += "12345";
		}
		SOPVariableFieldWriteable field = new SOPVariableFieldBuilder("TRADER").setDesc("交易员").setMinLength(53*5-2).setMaxLength(53*5 -1).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setAllowNull(false)
				.setValueMode(ValueMode.USER).build();
		byte[] bytes = field.format(m);
		System.out.println(Hex.toHexString(bytes));
		Object val = field.parse(ByteBuffer.wrap(bytes));
		Assert.assertEquals(m, val);
	}
	@Test
	public void testFormat6() throws TransformException {
		SOPVariableFieldWriteable field = new SOPVariableFieldBuilder("TRADER").setDesc("交易员").setMinLength(8).setMaxLength(8).setClazz(Date.class)
				.setFormatConverter(new Date2ByteArrayConverter()).setParseConverter(new ByteArray2DateConverter()).setAllowNull(false)
				.setValueMode(ValueMode.USER).build();
		String hex = "083230313530313031";
		byte[] bytes = field.format(DateUtil.toDate("20150101"));
		System.out.println(Hex.toHexString(bytes));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString(hex), bytes));
	}
}
