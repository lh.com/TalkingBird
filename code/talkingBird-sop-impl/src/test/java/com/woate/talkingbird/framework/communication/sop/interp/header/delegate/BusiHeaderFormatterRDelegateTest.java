package com.woate.talkingbird.framework.communication.sop.interp.header.delegate;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.interp.header.delegate.BusiHeaderFormatterDelegate;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldBuilder;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldImpl;
import com.woate.talkingbird.framework.core.AliasNameManager;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
import com.woate.talkingbird.framework.util.Hex;

public class BusiHeaderFormatterRDelegateTest {
	@Test
	public void testFormat1(){
		SOPSession.destory();
		BusiDataMetadateLoader loader = new BusiDataMetadateLoader(){
			@Override
			public BusiDataMetadateLoader load() {
				return this;
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				List<SOPMetadata> list = new ArrayList<SOPMetadata>();
				SOPFixedField field1 = new SOPFixedFieldBuilder("name").setAlias("姓名").setLength(5).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				list.add(field1);
				return list;
			}

			@Override
			public String getTradeCode() {
				return null;
			}

			@Override
			public List<SOPMetadata> list(PacketType type) {
				return null;
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getHeaderAliasNameManager() {
				// TODO 自动生成的方法存根
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}
		};
		BusiHeaderFormatterDelegate formatter = new BusiHeaderFormatterDelegate(loader);
		ByteBuffer buffer = null;
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		
		SOPBusiWrapper busiWrapper = session.createBusiData();
		busiWrapper.setAttribute("name", "123");
		try {
			buffer = formatter.format();
		} catch (TransformException e) {
			e.printStackTrace();
		}
		System.out.println(Hex.toDisplayString(buffer.array()));
		
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("0000313233"), buffer.array()));
		SOPSession.destory();
	}
	@Test
	public void testFormat2(){
		SOPSession.destory();
		BusiDataMetadateLoader loader = new BusiDataMetadateLoader(){
			@Override
			public BusiDataMetadateLoader load() {
				return this;
			}
			
			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				List<SOPMetadata> list = new ArrayList<SOPMetadata>();
				SOPFixedField field1 = new SOPFixedFieldBuilder("name").setAlias("姓名").setLength(5).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				list.add(field1);
				return list;
			}

			@Override
			public String getTradeCode() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<SOPMetadata> list(PacketType type) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getHeaderAliasNameManager() {
				// TODO 自动生成的方法存根
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}
		};
		BusiHeaderFormatterDelegate formatter = new BusiHeaderFormatterDelegate(loader);
		ByteBuffer buffer = null;
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		SOPBusiWrapper busiWrapper = session.createBusiData();
		busiWrapper.setAttribute("name", "12345");
		try {
			buffer = formatter.format();
		} catch (TransformException e) {
			e.printStackTrace();
		}
		System.out.println(Hex.toDisplayString(buffer.array()));
		
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("3132333435"), buffer.array()));
		SOPSession.destory();
	}
	@Test
	public void testFormat3() throws TransformException{
		SOPSession.destory();
		SOPFixedField field1 = new SOPFixedFieldBuilder("name").setAlias("姓名").setLength(5).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		BusiHeaderFormatterDelegate formatter = new BusiHeaderFormatterDelegate(null);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "123");
		byte[] bytes = formatter.format0(field1, map);
		System.out.println(Hex.toDisplayString(bytes));
		
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("0000313233"), bytes));
		SOPSession.destory();
	}
	@Test
	public void testFormat4() throws TransformException{
		SOPSession.destory();
		SOPFixedField field1 = new SOPFixedFieldBuilder("name").setAlias("姓名").setLength(5).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setRightFill(true).setValueMode(ValueMode.USER).build();
		BusiHeaderFormatterDelegate formatter = new BusiHeaderFormatterDelegate(null);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "123");
		byte[] bytes = formatter.format0(field1, map);
		System.out.println(Hex.toDisplayString(bytes));
		
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("3132330000"), bytes));
		SOPSession.destory();
	}
	@Test
	public void testFormat5() throws TransformException{
		SOPSession.destory();
		SOPFixedField field1 = new SOPFixedFieldBuilder("name").setAlias("姓名").setLength(6).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		BusiHeaderFormatterDelegate formatter = new BusiHeaderFormatterDelegate(null);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "123456");
		byte[] bytes = formatter.format0(field1, map);
		System.out.println(Hex.toDisplayString(bytes));
		
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("313233343536"), bytes));
		SOPSession.destory();
	}
	
}
