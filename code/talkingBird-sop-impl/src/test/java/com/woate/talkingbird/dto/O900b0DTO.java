package com.woate.talkingbird.dto;

public class O900b0DTO {
	private String secretKey; // 密钥
	private String cutTrans1; // 删减交易1
	private String lawFile1; // 法律文书号1
	private String workKey; // 工作密钥
	private String cutTrans2; // 删减交易2
	private String lawFile2; // 法律文书号2
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public String getCutTrans1() {
		return cutTrans1;
	}
	public void setCutTrans1(String cutTrans1) {
		this.cutTrans1 = cutTrans1;
	}
	public String getLawFile1() {
		return lawFile1;
	}
	public void setLawFile1(String lawFile1) {
		this.lawFile1 = lawFile1;
	}
	public String getWorkKey() {
		return workKey;
	}
	public void setWorkKey(String workKey) {
		this.workKey = workKey;
	}
	public String getCutTrans2() {
		return cutTrans2;
	}
	public void setCutTrans2(String cutTrans2) {
		this.cutTrans2 = cutTrans2;
	}
	public String getLawFile2() {
		return lawFile2;
	}
	public void setLawFile2(String lawFile2) {
		this.lawFile2 = lawFile2;
	}
	
}
