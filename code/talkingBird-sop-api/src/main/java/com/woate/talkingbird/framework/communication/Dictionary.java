/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication;
/**
 * 通信字段词典
 * @author liucheng
 * 2014-12-23 下午03:34:34
 *
 */
public class Dictionary {
	//---------------系统信息头---------------------
	/**
	 * 数据包长度
	 */
	public static final String SHJBCD = "SHJBCD";
	/**
	 * 报文MAC
	 */
	public static final String BAWMAC = "BAWMAC";
	/**
	 * MAC机构号
	 */
	public static final String MACJGH = "MACJGH";
	/**
	 * PIN种子
	 */
	public static final String PINZHZ = "PINZHZ";
	/**
	 * 渠道来源
	 */
	public static final String CHNLNO = "CHNLNO";
	/**
	 * 渠道去向
	 */
	public static final String CHNLTO = "CHNLTO";
	/**
	 * 加密标志
	 */
	public static final String MACFLG = "MACFLG";
	/**
	 * PIN标志
	 */
	public static final String PINFLG = "PINFLG";
	/**
	 * 组合标志
	 */
	public static final String COMFLG = "COMFLG";
	/**
	 * 主机服务名
	 */
	public static final String ZUJFUW = "ZUJFUW";
	/**
	 * 信息结束标志
	 */
	public static final String XXJSBZ = "XXJSBZ";
	/**
	 * 报文序号
	 */
	public static final String SJBSXH = "SJBSXH";
	/**
	 * 校验标志
	 */
	public static final String JIOYBZ = "JIOYBZ";
	/**
	 * 密钥版本号
	 */
	public static final String MIYBBH = "MIYBBH";
	/**
	 * 系统标识符
	 */
	public static final String QUDAOO = "QUDAOO";
	/**
	 * 密码偏移量
	 */
	public static final String MIMPYL = "MIMPYL";
	/**
	 * 帐号偏移量
	 */
	public static final String ZHHPYL = "ZHHPYL";
	//---------------公共信息头---------------------
	/**
	 * 终端号
	 */
	public static final String ZHNGDH = "ZHNGDH";
	/**
	 * 城市代码
	 */
	public static final String CHSHDM = "CHSHDM";
	/**
	 * 机构代码
	 */
	public static final String YNGYJG = "YNGYJG";
	/**
	 * 交易柜员
	 */
	public static final String JIO1GY = "JIO1GY";
	/**
	 * 联动交易码
	 */
	public static final String LDJYDM = "LDJYDM";
	/**
	 * 交易日期
	 */
	public static final String JIOYRQ = "JIOYRQ";
	/**
	 * 交易时间
	 */
	public static final String JIOYSJ = "JIOYSJ";
	/**
	 * 柜员流水号	
	 */
	public static final String GUIYLS = "GUIYLS";
	/**
	 * 出错交易序号
	 */
	public static final String CWJYXH = "CWJYXH";
	/**
	 * 错误代号
	 */
	public static final String PTCWDH = "PTCWDH";
	//---------------交易数据头---------------------
	/**
	 * 交易代码
	 */
	public static final String JIAOYM = "JIAOYM";
	/**
	 * 交易子码
	 */
	public static final String JIOYZM = "JIOYZM";
	/**
	 * 交易模式
	 */
	public static final String JIOYMS = "JIOYMS";
	/**
	 * 交易序号
	 */
	public static final String JIOYXH = "JIOYXH";
	/**
	 * 本交易包长度
	 */
	public static final String COMMLN = "COMMLN";
	/**
	 * 系统偏移1
	 */
	public static final String PNYIL1 = "PNYIL1";
	/**
	 * 系统偏移2
	 */
	public static final String PNYIL2 = "PNYIL2";
	/**
	 * 前台流水号
	 */
	public static final String QANTLS = "QANTLS";
	/**
	 * 前台日期
	 */
	public static final String QANTRQ = "QANTRQ";
	/**
	 * 授权柜员
	 */
	public static final String SHOQGY = "SHOQGY";
	/**
	 * 授权密码
	 */
	public static final String SHOQMM = "SHOQMM";
	/**
	 * 授权柜员有无卡标志
	 */
	public static final String YWKABZ = "YWKABZ";
	/**
	 * 授权柜员卡序号
	 */
	public static final String CZYNXH = "CZYNXH";
	//-----------------SOP转换参数------------------
	/**
	 * SOP编排或者反编排包的类型<br>
	 * @see #PACKET_TYPE
	 */
	public static final String PACKET_TYPE = "PACKET_TYPE";
	/**
	 * SOP报文对象
	 */
	public static final String PACKET_SOP_MSG = "PACKET_SOP_MSG";
}
