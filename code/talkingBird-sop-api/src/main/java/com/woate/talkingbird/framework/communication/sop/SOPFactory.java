/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop;


/**
 * SOP工厂类
 * @author lenovo
 *
 */
public abstract class SOPFactory {
	/**
	 * 获取接收型SOP包对象
	 * @param name SOP包别名或者交易码
	 * @return SOP包对象
	 */
	public abstract SOPPacket getSOPPacketR(String name);
	/**
	 * 获取发送型SOP包对象
	 * @param name SOP包别名或者交易码
	 * @return SOP包对象
	 */
	public abstract SOPPacket getSOPPacketS(String name);
	/**
	 * 加载元信息到加载器中
	 */
	public abstract SOPFactory load();
	public static SOPFactory getDefaultFactory(){
		try {
			@SuppressWarnings("unchecked")
			Class<SOPFactory> clazz = (Class<SOPFactory>) Class.forName("com.woate.talkingbird.framework.communication.sop.impl.DefaultSOPFactoryImpl");
			return clazz.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
