/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/**
 * 业务数据包装类
 * @author liucheng
 * 2014-12-18 下午02:18:23
 *
 */
public class SOPBusiWrapper {
	Map<String, Object> attributes = new HashMap<String, Object>();
	Map<String, Object> busidatas = new HashMap<String, Object>();

	public int attributesSize() {
		return attributes.size();
	}

	public Set<String> busidatasKeyset() {
		return busidatas.keySet();
	}

	public Set<String> attributesKeyset() {
		return attributes.keySet();
	}

	public void setAttribute(String name, Object val) {
		attributes.put(name, val);
	}
	public void setBusidata(String name, Object val) {
		busidatas.put(name, val);
	}

	public int busidataSize() {
		return busidatas.size();
	}

	@SuppressWarnings("unchecked")
	public <T> T getAttribute(String name) {
		return (T) attributes.get(name);
	}
	@SuppressWarnings("unchecked")
	public <T> T getBusidata(String name) {
		return (T) busidatas.get(name);
	}
	
	public Map<String, Object> attributes(){
		return attributes;
	}
	public Map<String, Object> busidatas(){
		return busidatas;
	}

	@Override
	public String toString() {
		return "SOPBusiWrapper [attributes=" + attributes + ", busidatas=" + busidatas + "]";
	}
	
}