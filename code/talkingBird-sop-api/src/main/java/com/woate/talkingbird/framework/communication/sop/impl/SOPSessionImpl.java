/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.impl;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
/**
 * SOP会话实现
 * @author liucheng
 * 2014-12-19 上午08:29:09
 *
 */
public class SOPSessionImpl extends SOPSession {
	final Map<String, Object> attributes = new HashMap<String, Object>();
	final Deque<SOPBusiWrapper> datas = new LinkedList<SOPBusiWrapper>();
	@Override
	public SOPBusiWrapper createBusiData() {
		SOPBusiWrapper wrapper = new SOPBusiWrapper();
		this.datas.add(wrapper);
		return wrapper;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getAttribute(String name) {
		return (T) attributes.get(name);
	}

	@Override
	public Deque<SOPBusiWrapper> busiDatas() {
		return this.datas;
	}

	@Override
	public void setAttribute(String name, Object val) {
		this.attributes.put(name, val);
	}

	@Override
	public Map<String, Object> attributes() {
		return this.attributes;
	}

	@Override
	public SOPBusiWrapper peekFirstBusiData() {
		return this.datas.peekFirst();
	}

	@Override
	public SOPBusiWrapper peekLastBusiData() {
		return this.datas.peekLast();
	}

	@Override
	public SOPBusiWrapper pollFirstBusiData() {
		return this.datas.pollFirst();
	}

	@Override
	public SOPBusiWrapper pollLastBusiData() {
		return this.datas.pollLast();
	}

	@Override
	public String toString() {
		return "SOPSessionImpl [attributes=" + attributes + ", datas=" + datas + "]";
	}
	
}
